﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HNApp.Models
{

    public class ExternalLoginConfirmationViewModel
    {
        [Display(Name = "Email")]
        [Required]
        public string Email
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }


    public class ResetPasswordViewModel
    {
        public string Code
        {
            get;
            set;
        }

        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword
        {
            get;
            set;
        }

        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password
        {
            get;
            set;
        }
        
    }


    public class RegisterViewModel
    {
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword
        {
            get;
            set;
        }

        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        public string Fullname
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        public string Password
        {
            get;
            set;
        }

        [DataType(DataType.PhoneNumber)]
        [Required]
        public string PhoneNumber
        {
            get;
            set;
        }
        
    }


    public class LoginViewModel
    {
        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "Remember me?")]
        public bool RememberMe
        {
            get;
            set;
        }
        
    }



    public class ForgotPasswordViewModel
    {
        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

      
    }


    public class ExternalLoginListViewModel
    {
        public string ReturnUrl
        {
            get;
            set;
        }
        
    }
}