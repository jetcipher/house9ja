﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace HNApp.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {

        }
        public string Address
        {
            get;
            set;
        }

        public string BanReason
        {
            get;
            set;
        }

        public string Facebook
        {
            get;
            set;
        }

        public virtual List<EntityString> FavouriteProperties
        {
            get;
            set;
        }

        public string Fullname
        {
            get;
            set;
        }

        public bool IsBanned
        {
            get;
            set;
        }

        public string LinkedIn
        {
            get;
            set;
        }

        public string ProfilePicPath
        {
            get;
            set;
        }

        public string Twitter
        {
            get;
            set;
        }

        public ApplicationUser(string username) : base(username)
        {

        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }



    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public DbSet<ServiceListing> ServiceListings { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<PropertyListing> PropertyListings { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Description> Descriptions { get; set; }
        public DbSet<Cordinates> Cordinates { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<CityLgaArea> CityLgaAreas { get; set; }
        public DbSet<Categorey> Categories { get; set; }
        public DbSet<BaseLocation> BaseLocations { get; set; }
        public DbSet<SiteSetting> SiteSettings { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
         //   base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Add<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Add<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

}