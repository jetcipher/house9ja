﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HNApp.Models
{
    public class AdminIndexViewModel
    {
        public long PropertyCount
        {
            get;
            set;
        }

        public long ServiceCount
        {
            get;
            set;
        }

        [EmailAddress]
        [Required]
        public string SiteEmail
        {
            get;
            set;
        }

        [Required]
        public string SitePhone
        {
            get;
            set;
        }

        public long UserCount
        {
            get;
            set;
        }

    }

    public class MyAccountViewModel
    {
        public string Address
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        public string ConfirmNewPassword
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Facebook
        {
            get;
            set;
        }

        [Required]
        public string Fullname
        {
            get;
            set;
        }

        public string LinkedIN
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        public string NewPassword
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }

        [Required]
        public string PhoneNumber
        {
            get;
            set;
        }

        public string ProfilePath
        {
            get;
            set;
        }

        public string Twitter
        {
            get;
            set;
        }

    }

    public class PropertiesViewModel
    {
        public string Area
        {
            get;
            set;
        }

        public string Categorey
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public ContractType? Contract
        {
            get;
            set;
        }

        public Facility? Facilities
        {
            get;
            set;
        }

        public IEnumerable<PropertyListing> Listings
        {
            get;
            set;
        }

        public double MaxPrice
        {
            get;
            set;
        }

        public double MinPrice
        {
            get;
            set;
        }

        public OrderBy? Order
        {
            get;
            set;
        }

        public int PageNo
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int Total
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        
    }


    public class PropertyViewModel
    {
        public bool CanEditDel
        {
            get;
            set;
        }

        public HNApp.Models.Categorey Categorey
        {
            get;
            set;
        }

        public PropertyListing Listing
        {
            get;
            set;
        }

        public List<PropertyListing> RelatedListings
        {
            get;
            set;
        }

        public ApplicationUser User
        {
            get;
            set;
        }

        
    }


    public class ServicesViewModel
    {
        public string Area
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public IEnumerable<ServiceListing> Listings
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public ServOrderBy? Order
        {
            get;
            set;
        }

        public int PageNo
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int Total
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }
        
    }


    public class ServiceViewModel
    {
        public ServiceListing Listing
        {
            get;
            set;
        }

        public string ServicesOffered
        {
            get;
            set;
        }

        public int TotalRating
        {
            get;
            set;
        }
        
    }

}