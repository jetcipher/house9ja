﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HNApp.Models
{
    public enum ServOrderBy
    {
        DateDesc,
        DateAsc,
        RatingAsc,
        RatingDesc
    }

    public interface IKey
    {
        int Id { get; set; }
    }

    public class KeyComaprer : IEqualityComparer<IKey>
    {
        public bool Equals(IKey x, IKey y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(IKey obj)
        {
            if (obj == null) return 0;
            //obj.Id.GetHashCode();
            return obj.GetHashCode();
        }
    }

    [Newtonsoft.Json.JsonConverter(typeof(EntityStringConverter))]
    public class TypeString : IKey
    {
        public int Id { get; set; }
        public string Value { get; set; }


        public static implicit operator TypeString(string value)
        {
            return new TypeString { Value = value };

        }


        public static implicit operator string(TypeString value)
        {
            return value.Value;
        }
    }


    public class ServiceListing : IKey
    {
        public string Address
        {
            get;
            set;
        }

        public string AuthorId
        {
            get;
            set;
        }

        public virtual HNApp.Models.Contact Contact
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string ImagePath
        {
            get;
            set;
        }

        public virtual HNApp.Models.Location Location
        {
            get;
            set;
        }

        public virtual List<Review> Reviews
        {
            get;
            set;
        }

        public virtual List<EntityString> ServiceIds
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public bool IsVerified { get; set; }

        public string Url
        {
            get;
            set;
        }

    }


    public class Service : IKey
    {
        public DateTime Date
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

    }




    public class Review : IKey
    {
        public int Id { get; set; }
        public string AuthorId
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Rating
        {
            get;
            set;
        }

    }



    public class PropertyListing : IKey
    {
        public string AdditionalText
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string AuthorId
        {
            get;
            set;
        }

        public int CategoreyId
        {
            get;
            set;
        }

        public virtual HNApp.Models.Contact Contact
        {
            get;
            set;
        }

        public ContractType Contract
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public virtual List<Description> Descriptions
        {
            get;
            set;
        }

        public Facility Facilities
        {
            get;
            set;
        }

        public string FilePath
        {
            get;
            set;
        }

       
        public int Id
        {
            get;
            set;
        }

        public virtual HNApp.Models.Location Location
        {
            get;
            set;
        }

        public virtual List<EntityString> Photos
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int TypeId
        {
            get;
            set;
        }

        public string VideoPath
        {
            get;
            set;
        }

        public int ViewCount
        {
            get;
            set;
        }

        public string VirtualInspectPath
        {
            get;
            set;
        }

    }



    public enum OrderBy 
    {
        DateDesc,
        DateAsc,
        PriceAsc,
        PriceDesc
    }


    public class Location : IKey
    {
        public int Id { get; set; }
        public string Area
        {
            get;
            set;
        }

        public string CityLGA
        {
            get;
            set;
        }

        public virtual Cordinates Cordinate
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

    }

    [Flags]
    public enum Facility
    {
        Schools = 1,
        Hospitals = 2,
        Restaurants = 4,
        ClubsAndBars = 8,
        Banks = 16,
        FillingStation = 32,
        Industries = 64,
        Park = 128,
        Electricity = 256,
        Water = 512,
        Security = 1024
    }


    public class Description : IKey
    {
        public int Id { get; set; }
        public string Name
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

    }


    public class Cordinates : IKey
    {
        public int Id { get; set; }
        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

    }


    public enum ContractType
    {
        Rent,
        Sale
    }


    public enum ContactSource
    {
        Agent,
        Owner
    }


    public class Contact : IKey
    {
        public int Id { get; set; }
        public string Address
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public ContactSource Source
        {
            get;
            set;
        }


    }


    public class CityLgaArea : IKey
    {
        public virtual List<EntityString> Area
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Id { get; set; }

    }


    public class Categorey : IKey
    {
      
        public virtual List<EntityString> AllowedDescriptions
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

       
        public virtual List<TypeString> Types
        {
            get;
            set;
        }

    }

    public class BaseLocation : IKey
    {
        public virtual List<EntityString> AreaPredictions
        {
            get;
            set;
        }

        public virtual List<CityLgaArea> CityLga
        {
            get;
            set;
        }

       
        public int Id
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

    }

    public class SiteSetting : IKey
    {
        public string Email
        {
            get;
            set;
        }

     
        public int Id
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

    }



}

