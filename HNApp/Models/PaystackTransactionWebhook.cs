﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace HNApp.Models
{
    public class PaystackTransactionWebhook
    {
        [JsonProperty("event")]
        public string Event { get; set; }
        [JsonProperty("data")]
        public PayStack.Net.TransactionList.Datum Data { get; set; }
    }
}