﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace HNApp
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        public const string ApiKey = "az128edjfnfkldihe";
    
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Contains("ApiKey"))
            {
                var key = actionContext.Request.Headers.GetValues("ApiKey").FirstOrDefault();
                if (key == ApiKey) return true;
            }
            return base.IsAuthorized(actionContext);
        }


        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
        }

    }
}