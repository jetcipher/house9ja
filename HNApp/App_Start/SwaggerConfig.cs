using System.Web.Http;
using WebActivatorEx;
using HNApp;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace HNApp
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    
                    c.SingleApiVersion("v1", "House9ja");

                  
                    //c.BasicAuth("basic")
                    //    .Description("Basic HTTP Authentication");
                    ////
                    // NOTE: You must also configure 'EnableApiKeySupport' below in the SwaggerUI section
                    c.ApiKey("ApiKey")
                        .Description("API Key Authentication")
                        .Name("ApiKey")
                        .In("header");
                  
                    c.IncludeXmlComments(GetXmlCommentsPath());
                 
                    
                })

                .EnableSwaggerUi(c =>
                {
                    
                    c.DocumentTitle("House9ja Api");
                    
                    c.EnableApiKeySupport("ApiKey", "header");
                });
        }


        protected static string GetXmlCommentsPath()
        {
            return System.String.Format(@"{0}\bin\HNApp.XML", System.AppDomain.CurrentDomain.BaseDirectory);
        }


    }
}
