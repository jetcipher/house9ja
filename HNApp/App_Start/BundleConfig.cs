﻿using System.Web;
using System.Web.Optimization;

namespace HNApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/Site.js"));

            bundles.Add(new ScriptBundle("~/js/angular").Include("~/Scripts/angular.js",
                       "~/Scripts/angular-animate.js",
                       "~/Scripts/angular-aria.js",
                       "~/Scripts/angular-resource.js",
                       "~/Scripts/angular-material.js",
                       "~/Scripts/angular-messages.js",
                       "~/Scripts/odataresources.min.js",
                       "~/Scripts/angular.rangeSlider.js",
                       "~/Scripts/App.js"));

           

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/angular-material.css",

                      "~/Content/site-style.css"));
            BundleTable.EnableOptimizations = true;
        }
    }
}
