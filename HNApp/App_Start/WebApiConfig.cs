﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Web.Http.OData.Extensions;
using CacheCow.Server;
using CacheCow.Common;
using System.Configuration;

namespace HNApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            //IEntityTagStore etagStore = new Cache.MongoDbEntityTagStore(ConfigurationManager.AppSettings["connectionString"], ConfigurationManager.AppSettings["dbName"]);
            //var cacheCowCacheHandler = new CachingHandler(config, etagStore) { AddLastModifiedHeader = true };
            //config.MessageHandlers.Add(cacheCowCacheHandler);
            config.AddODataQueryFilter();
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
