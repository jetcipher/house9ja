﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using HNApp.Models;
using System.Web.Mvc;
using System.Linq;
using System.IO;
using System.Web.Mvc.Html;
using System.Configuration;

namespace HNApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public AccountController()
        {

        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        
        public async Task<ActionResult> Index(string msg = null)
        {
           if(!string.IsNullOrEmpty(msg)) ViewBag.Msg = msg;
            var user = User.Identity.GetUser();
            
            return View(new MyAccountViewModel { Address = user.Address , Email = user.Email , Facebook = user.Facebook , Fullname = user.Fullname , LinkedIN = user.LinkedIn , PhoneNumber = user.PhoneNumber , ProfilePath = user.ProfilePicPath , Twitter = user.Twitter });
        }

        public async Task<ActionResult> UpdateProfilePic(string url)
        {
            
            if (!string.IsNullOrEmpty(url))
            {
               
                var user = User.Identity.GetUser();
                var oldpath = (string)user.ProfilePicPath.Clone();
                user.ProfilePicPath = url;
                UserManager.Update(user);
                Api.FilesController fc = new Api.FilesController();
                if (new Uri(user.ProfilePicPath).IsAbsoluteUri) fc.Delete(oldpath);
            }
            return RedirectToAction("Index",new { msg = "Profile picture changed."});
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(MyAccountViewModel mav)
        {
            var user = User.Identity.GetUser();
            if (!string.IsNullOrEmpty(mav.NewPassword))
            {
                if (string.IsNullOrEmpty(mav.Password)) ModelState.AddModelError("Password", "Please provide your current password");
                else if (string.IsNullOrEmpty(mav.ConfirmNewPassword)) ModelState.AddModelError("ConfirmNewPassword", "Please provide a matching password");
                else
                {
                    
                    if (UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, mav.Password) != PasswordVerificationResult.Success) ModelState.AddModelError("Password", "The current password is wrong");
                    else if(mav.NewPassword != mav.ConfirmNewPassword)
                    {
                        ModelState.AddModelError("NewPassword", "Passwords dont match.");
                        ModelState.AddModelError("ConfirmNewPassword", "Passwords dont match.");
                    }
                    else
                    {
                        var result = UserManager.ResetPassword(user.Id, UserManager.GeneratePasswordResetToken(user.Id), mav.NewPassword);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.LastOrDefault());
                            return View(mav);
                        }
                    }
                } 
            }
            if (ModelState.IsValid)
            {
                
                user.Fullname = mav.Fullname;
                user.Facebook = mav.Facebook;
                user.Address = mav.Address;
                user.PhoneNumber = mav.PhoneNumber;
                user.Twitter = mav.Twitter;
                user.LinkedIn = mav.LinkedIN;
                UserManager.Update(user);
                return RedirectToAction("Index", new { msg = "Profile updated" });
            }

            return View(mav);
        }

        
        public ActionResult Listings(string query = "")
        {
               
                ViewBag.pgNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
            var id = User.Identity.GetUserId();
                var lis =(string.IsNullOrEmpty(query)) ? db.PropertyListings.Where(a => a.AuthorId == id) : db.PropertyListings.Where(a=>a.AuthorId == id && a.Title.ToLower().Contains(query.ToLower()));
                ViewBag.ItemCount = lis.Count();
                return View(lis.OrderByDescending(a=>a.Id).Skip((int)(ViewBag.pgNo * 10) - 10).Take(10).ToList());
           
        }


        public ActionResult Services()
        {
         

            ViewBag.pgNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
            var id = User.Identity.GetUserId();
            var lis =  db.ServiceListings.Where(a => a.AuthorId == id);
            ViewBag.ItemCount = lis.Count();
            return View(lis.OrderByDescending(a => a.Id).Skip((int)(ViewBag.pgNo * 10) - 10).Take(10).ToList());
        }
        
        public ActionResult RemoveFav(string id)
        {
            var user = User.Identity.GetUser();
            user.FavouriteProperties.Remove(id);
            UserManager.Update(user);
            return RedirectToAction("Favourite");
        }
        public ActionResult Favourite()
        {
            
            ViewBag.pgNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
            var lis = User.Identity.GetUser().FavouriteProperties?.Select(a => db.PropertyListings.Find(int.Parse(a)));
            ViewBag.ItemCount = lis?.Count() ?? 0;
            return View(lis?.OrderByDescending(a=>a.Id).Skip((int)(ViewBag.pgNo * 10) - 10).Take(10).ToList() ?? new List<PropertyListing>());
        }





        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid email and or password");
                    return View(model);
            }
        }

       
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser(model.Email) { Email = model.Email,Fullname = model.Fullname,PhoneNumber = model.PhoneNumber, ProfilePicPath = "/Images/defaultProfile.jpg" };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                    await UserManager.SendEmailAsync(user.Id, "Confirm your account", GetConfirmMailStr(callbackUrl,code,user.Fullname));


                    return RedirectToAction("RegisterSuccess", "Account");
                }
                ModelState.AddModelError("", result.Errors.LastOrDefault());
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        public ActionResult RegisterSuccess()
        {
            return View();
        }


        public async Task<ActionResult> SendCode()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

            await UserManager.SendEmailAsync(user.Id, "Confirm your account", GetConfirmMailStr(callbackUrl, code, user.Fullname));


            return RedirectToAction("Index", "account");
        }


        private string GetConfirmMailStr(string url,string code,string name)
        {
            string body = "";
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Mail/Confirm.html")))
            {
                body = reader.ReadToEnd();
            }
            return body.Replace("{url}", url).Replace("{code}",code).Replace("{name}",name);
        }


        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (!result.Succeeded) return View("Error"); else return RedirectToActionPermanent("index");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

       
        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            
            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    return await ExternalLoginConfirmation(new ExternalLoginConfirmationViewModel { Email = loginInfo.Email, Name = loginInfo.ExternalIdentity.Name }, returnUrl);
            }
        }


        [Route("signin-google")]
        [AllowAnonymous]
        public async Task<ActionResult> GoogleLoginCallback(string returnUrl)
        {

            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }


            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.Failure:
                default:
                    
                    return await ExternalLoginConfirmation(new ExternalLoginConfirmationViewModel { Email = loginInfo.Email, Name = loginInfo.ExternalIdentity.Name } , returnUrl);
            }
        }


      
        [AllowAnonymous]
        private async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "account");
            }
            
            if (ModelState.IsValid)
            {
                
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser(model.Email) { Email = model.Email , Fullname = model.Name , ProfilePicPath = "/Images/defaultProfile.jpg" };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View("register", new RegisterViewModel { Email = model.Email, Fullname = model.Name });
        }

       
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                context.RequestContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
