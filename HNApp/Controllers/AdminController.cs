﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HNApp.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.IO;
using static HNApp.Utils.Helpers;

namespace HNApp.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
           // var col = db.GetCollection<SiteSetting>("setting");
            var uount = UserManager.Users.Count();
            var pcount = db.PropertyListings.Count();
            var scount = db.ServiceListings.Count();
            var setting = db.SiteSettings.FirstOrDefault();

            return View(new AdminIndexViewModel { PropertyCount = pcount, ServiceCount = scount, SiteEmail = setting?.Email, SitePhone = setting?.PhoneNumber, UserCount = uount });
        }

        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult Index(AdminIndexViewModel avm)
        {
            var uount = UserManager.Users.Count();
            var pcount = db.PropertyListings.Count();
            var scount = db.ServiceListings.Count();
            var setting = db.SiteSettings.Find(0);
            if (ModelState.IsValid)
            {
                if (setting == null) db.SiteSettings.Add(new Models.SiteSetting { Email = avm.SiteEmail, PhoneNumber = avm.SitePhone });
                else
                {
                    setting.Email = avm.SiteEmail;
                    setting.PhoneNumber = avm.SitePhone;
                    db.Entry(setting).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }


        public ApplicationUserManager UserManager
        {
            get
            {

                return HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
        }



        public ApplicationRoleManager RoleManager { get { return HttpContext.GetOwinContext().Get<ApplicationRoleManager>(); } }

        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> AddAdmin()
        {
            
                var id = User.Identity.GetUserId();
                var user = await UserManager.FindByIdAsync(id);
                await RemoveRole("admin", id);
                await RemoveRole("moderator", id);
                await AddtoRole("admin",id);
           
           
            return RedirectToAction("Index");
        }


        public ActionResult Categories()
        {
            return View();
        }


        public ActionResult Users(string query = null)
        {
            
            if (string.IsNullOrEmpty(query))
            {
                
                ViewBag.pgNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
                ViewBag.ItemCount = UserManager.Users.Count();
                return View(UserManager.Users.OrderByDescending(a=>a.Id).Skip((int)(ViewBag.pgNo * 10) - 10).Take(10).ToList());
            }
            else
            {
                var users = UserManager.Users.Where(a => a.Email.ToLower() == query || a.Fullname.ToLower().Contains(query.ToLower()));
                ViewBag.Query = query;
                ViewBag.pgNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
                ViewBag.ItemCount = users.Count();
                return View(users.OrderByDescending(a => a.Id).Skip((int)(ViewBag.pgNo * 10) - 10).Take(10).ToList());
            }
                
        }

        private async Task AddtoRole(string roleName,string userId)
        {
           
                var result = await RoleManager.FindByNameAsync(roleName);
                if (result == null) await RoleManager.CreateAsync(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole(roleName));
                await UserManager.AddToRoleAsync(userId, roleName);
            
            
        }

        private async Task RemoveRole(string roleName, string userId)
        {
            var result = await RoleManager.FindByNameAsync(roleName);
            if (result == null) await RoleManager.CreateAsync(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole(roleName));
            await UserManager.RemoveFromRoleAsync(userId, roleName);
        }

        public async Task<ActionResult> Promote(string query,string role,string Id)
        {
            
            var user = await UserManager.FindByIdAsync(Id);
           await RemoveRole("admin",user.Id);
            await RemoveRole("moderator",user.Id);
            UserManager.Update(user);
            if (!string.IsNullOrEmpty(role)) await AddtoRole(role, user.Id);
            await UserManager.SendEmailAsync(user.Id, "User account status changed", GetMailStr($"Your account status has been changed to <strong>{role}</strong> you can now enjoy the benefits of the account type", user.Fullname));
            return Redirect(query);
        }

        

        public async Task<ActionResult> BanUnban(string query, bool ban, string Id,string reason)
        {
            var user = await UserManager.FindByIdAsync(Id);
            user.IsBanned = ban;
            user.BanReason = reason;
            await UserManager.UpdateAsync(user);
            string subject = (ban == true) ? "Your Account Has Been Banned" : "Your Account Has Been Un-Banned";
            string msg = (ban == true) ? $"Your have been banned from house9ja the reason is <br/> <strong>{reason}</strong>" : "Your have been unbanned from house9ja you can now login";
            await UserManager.SendEmailAsync(user.Id, subject, GetMailStr(msg, user.Fullname));
            return Redirect(query);
        }


        public ActionResult Stypes()
        {
            
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}