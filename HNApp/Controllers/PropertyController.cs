﻿using HNApp.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HNApp.Utils;
using Microsoft.AspNet.Identity;
using System.Web.Mvc.Html;
using System.Text;
using System.Linq.Expressions;

namespace HNApp.Controllers
{
    
    public class PropertyController : Controller
    {
        private ApplicationUserManager _userManager;
        // GET: Listing

        private ApplicationDbContext db = new ApplicationDbContext();

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Route("properties/{categorey}/{type}")]
        public ActionResult Index(string categorey,string type, string state, string city,string area, OrderBy? order, int? contract,string priceRange,Facility? facilities)
        {

            IQueryable<PropertyListing> result = null;
            Tuple<int,int> ids; 
            try
            {
                
                ids = GetCat(categorey.Replace('-',' '), type.Replace('-',' '));
                result = (ids.Item2 == -1) ?  db.PropertyListings.Where(a => a.CategoreyId == ids.Item1) : db.PropertyListings.Where(a => a.CategoreyId == ids.Item1 && a.TypeId == ids.Item2);
            }
            catch (Exception e) when (e.Message.Contains("find all"))
            {
                result = db.PropertyListings;
            }
            catch(Exception e)
            {
                return new HttpNotFoundResult("Cannot find categorey");
            }
            
            var title = type.Replace('-',' ') + " " + categorey.Replace('-',' ');
            title = (title.ToLower().Contains("all all")) ? "Properties" : title;
            PropertiesViewModel pvm = new PropertiesViewModel { Type = type.Replace('-', ' '), Categorey = categorey.Replace('-', ' ') };
          
            try
            {
                var fl = FilterLocation(ref result, state?.Replace('_', ' ') ?? "All", city?.Replace('_', ' ') ?? "All", area?.Replace('_', ' ') ?? "All");
                
                title += " in " + fl;
                pvm.State = state;
                pvm.City = city;
                pvm.Area = area;
            }
            catch {
                title += " in Nigeria";
            }

            if (contract != null)
            {
                result = result.Where(a => a.Contract == (ContractType)contract.Value);
                pvm.Contract = (ContractType)contract;
            }

            try
            {
                if (!string.IsNullOrEmpty(priceRange))
                {
                    var fp = FilterPrice(ref result,priceRange);
                    pvm.MinPrice = fp.Item1;
                    pvm.MaxPrice = fp.Item2;
                }
            }
            catch {}


            //try
            //{
            //    if (facilities != null) result = result.Match<PropertyListing>(a => (Facility.Banks & a.Facilities) == Facility.Banks);
            //}
            //catch{}

            switch (order)
            {
                case OrderBy.DateAsc:
                    result = result.OrderBy(a => a.Date);
                    break;
                case OrderBy.DateDesc:
                    result = result.OrderByDescending(a => a.Date);
                    break;
                case OrderBy.PriceAsc:
                    result = result.OrderBy(a => a.Price);
                    break;
                case OrderBy.PriceDesc:
                    result = result.OrderByDescending(a => a.Price);
                    break;
                default:
                    result = result.OrderByDescending(a => a.Date);
                    break;
            }


            pvm.Order = order  == null ? OrderBy.DateDesc : order;
            

            try
            {
                result = result.Where(a => (facilities & a.Facilities) == facilities);
                pvm.Facilities = facilities;
            }
            catch { }
            
            pvm.PageNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
            pvm.Total = result.Count();

            var skipper = (pvm.PageNo - 1) * 10;
            var data = result.Skip(skipper).Take(10).ToList();
            pvm.Title = title;
            pvm.Listings = data;
            return View(pvm);
        }

        

        private string FilterLocation(ref IQueryable<PropertyListing> store, string state,string city,string area)
        {
            string title = "";
            int i = 0;
            if(area.ToLower() != "all")
            {
                store = store.Where(a => a.Location.Area.ToLower().Contains(area.ToLower()));
                title += area+" ";
                i++;
            }
            if (city.ToLower() != "all")
            {
                store = store.Where(a => a.Location.CityLGA.ToLower().Contains(city.ToLower()));
                title += city+" ";
                i++;

            }
            if (state.ToLower() != "all")
            {
                store = store.Where(a => a.Location.State.ToLower().Contains(state.ToLower()));
                title += state;
                i++;
            }

            if (title == "") title = "Nigeria";
            if (i >= 1) return title;
            else return  title.Trim();
            
        }

        
        private Tuple<double,double>  FilterPrice(ref IQueryable<PropertyListing> store, string price)
        {
            var prices = price.Split('-');
            if (prices.Count() != 2) throw new ArgumentException("Price is invalid");
            double minprice,maxprice;
            if (double.TryParse(prices[0],out minprice) && double.TryParse(prices[1],out  maxprice))
            {
              store = store.Where(a => (a.Price >= minprice) && (a.Price <= maxprice));
               return new Tuple<double, double> ( minprice, maxprice);
            }
            throw new ArgumentException("Cannot parse price range");
        }


        
        private Tuple<int,int>  GetCat(string categorey,string type)
        {
            if (categorey.ToLower() == "all") throw new ArgumentException("find all");
            var cat = db.Categories.Where(a => a.Name.ToLower() == categorey.ToLower()).FirstOrDefault();
            var cate = (type.ToLower() != "all") ? (cat?.Types.Where(a => a.Value.ToLower() == type.ToLower()).FirstOrDefault() != null) ? cat : null  : cat;
            if (cate == null) throw new ArgumentException("categorey or type dosent exist");
            var index = (type != "all") ? cate.Types.FindIndex(a=>a.Value == type): -1;
            return new Tuple<int, int>(cate.Id, index);
        }


        [Route("property/{index}")]
        public async Task<ActionResult> Index(string index)
        {
            var idOb = ParseUrl(index);
            var id = idOb["id"];
            if (string.IsNullOrEmpty(id)) return Redirect("/home/index");



            var property = db.PropertyListings.Find(int.Parse(id));
            
            if (property == null) return Redirect("/home/index");
            try
            {
                return View("property", new PropertyViewModel { Categorey = db.Categories.Find(property.CategoreyId), Listing = property, RelatedListings = db.PropertyListings.Where(a => ((a.CategoreyId == property.CategoreyId && a.TypeId == property.TypeId) && (a.Location.State == property.Location.State && a.Location.CityLGA == property.Location.CityLGA)) && (a.Id != property.Id)).OrderBy(a=> Guid.NewGuid()).ToList(), User = await UserManager.FindByIdAsync(property.AuthorId), CanEditDel = (User.IsInRole("admin") || User.IsInRole("moderator") || property.AuthorId == User.Identity.GetUserId()) });
            }
            catch
            {
                return Redirect("/properties/all/all");
            }
           
        }

        [Authorize,Route("property/add")]
        public ActionResult Add()
        {
            return View();
        }


        [Authorize, Route("property/edit/{id}")]
        public ActionResult Edit(string id)
        {
            ViewBag.Id = id;
            return View();
        }

        [Authorize, Route("property/delete/{id}")]
        public ActionResult Delete(int id)
        {
            var prp = db.PropertyListings.Find(id);
            if (prp == null) return Redirect("services/all");
            if ((User?.Identity.GetUserId() != prp.AuthorId) || (!User.IsInRole("admin")) || (!User.IsInRole("moderator"))) Redirect("services/all");
            db.DeleteAll(prp);
            db.Entry(prp).State = System.Data.Entity.EntityState.Deleted;
           
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }



        [Authorize, Route("listing/mark")]
        public ActionResult Mark(string id, string returnurl)
        {
            var uid = User.Identity.GetUserId();
            var user = UserManager.FindById(uid);
            if (user.FavouriteProperties?.Count(a=>a.Value == id) >= 1)
            {
                user.FavouriteProperties?.RemoveAt(user.FavouriteProperties.FindIndex(a => a.Value == id));
                UserManager.Update(user);
            }
            else
            {
                if (user.FavouriteProperties != null) user.FavouriteProperties.Add(id);
                else user.FavouriteProperties = new List<EntityString> { id };
                UserManager.Update(user);
            }
            return Redirect(returnurl);
        }

        [Route("listing/increment")]
        public ActionResult IncrementCount(int id)
        {
            var list = db.PropertyListings.Find(id);
            if(list != null)
            {
                list.ViewCount += 1;
                db.Entry(list).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Content("ok");
        }

        private Dictionary<string,string> ParseUrl(string url)
        {
           var parts =  url.Split('-');
            if (parts.Count() == 1) return new Dictionary<string, string>() { ["id"] = string.Empty, ["title"] = string.Empty };
            var dic = new Dictionary<string, string>();
            dic.Add("id", parts[0]);
            var title = "";
            for (int i = 1; i < parts.Count(); i++)
            {
                title += parts[i] + " ";
            }
            dic.Add("title", title);
            return dic;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}