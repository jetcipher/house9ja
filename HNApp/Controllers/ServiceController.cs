﻿using HNApp.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using PayStack.Net;
using System.Threading.Tasks;
using HNApp.Utils;

namespace HNApp.Controllers
{
    [Authorize]
    public class ServiceController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Service
        [AllowAnonymous,Route("services/{type}")]
        public ActionResult Index(string type, string state, string city, string area,string name,ServOrderBy? order)
        {
            IQueryable<ServiceListing> result = null;
            try
            {
                var id = GetTypeId(type.Replace('-', ' '));
                if (id != -1) result = db.ServiceListings.Where(a => a.ServiceIds.Contains(id.ToString()));
                else result = db.ServiceListings;
            }
            catch {}


            var title = type.Replace('-', ' ');
            title = (title.ToLower().Contains("all")) ? "Services" : title;
            ServicesViewModel svm = new ServicesViewModel { Type = type.Replace('-', ' ') };


            try
            {
                var fl = FilterLocation(ref result,state?.Replace('_', ' ') ?? "All", city?.Replace('_', ' ') ?? "All", area?.Replace('_', ' ') ?? "All");
                 title += " in " + fl;
                svm.State = state;
                svm.City = city;
                svm.Area = area;
            }
            catch(Exception e)
            {
                title += " in Nigeria";
            }

            if (!string.IsNullOrEmpty(name))
            {
                result = result.Where(a => a.Title.ToLower().Contains(name.ToLower()));
                svm.Name = name;
            }

            switch (order)
            {
                case ServOrderBy.DateAsc:
                    result = result.OrderBy(a => a.Date);
                    break;
                case ServOrderBy.DateDesc:
                    result = result.OrderByDescending(a => a.Date);
                    break;
                case ServOrderBy.RatingAsc:
                    result = result.OrderBy(a => a.Reviews.Sum(b=>b.Rating));
                    break;
                case ServOrderBy.RatingDesc:
                    result = result.OrderBy(a => a.Reviews.Sum(b => b.Rating));
                    break;
                default:
                    result = result.OrderByDescending(a => a.Date);
                    break;
            }


            svm.Order = order == null ? ServOrderBy.DateDesc : order;

           
            svm.PageNo = Request.Form["pageNo"] == null ? 1 : int.Parse(Request.Form["pageNo"]);
            svm.Total = result.Count();

            var skipper = (svm.PageNo - 1) * 10;
            svm.Title = title;
            svm.Listings = result.Skip(skipper).Take(10).ToList();


            return View("Services",svm);
        }


        [Route("service/servicenames/{id}")]
        public ActionResult GetServString(int id)
        {
            
            var item = db.ServiceListings.Find(id);
            if (item == null) return Content("");
            try
            {
                string result = "";
                foreach (var it in item.ServiceIds)
                {
                    result += " " + db.Services.Find(int.Parse(it)).Title;
                }
                return Content(result.Trim());
            }
            catch (Exception e)
            {

              return Content("");
            }
            
        }



        private string FilterLocation(ref IQueryable<ServiceListing> store, string state, string city, string area)
        {
            string title = "";
            int i = 0;
            if (area.ToLower() != "all")
            {
                store = store.Where(a => a.Location.Area.ToLower().Contains(area.ToLower()));
                title += area + " ";
                i++;
            }
            if (city.ToLower() != "all")
            {
                store = store.Where(a => a.Location.CityLGA.ToLower().Contains(city.ToLower()));
                title += city + " ";
                i++;

            }
            if (state.ToLower() != "all")
            {
                store = store.Where(a => a.Location.State.ToLower().Contains(state.ToLower()));
                title += state;
                i++;
            }

            if (title == "") title = "Nigeria";
            if (i >= 1) return title;
            else return title.Trim();

        }




        [AllowAnonymous, Route("service/{index}")]
        public ActionResult Index(string index,string msg)
        {
            if (!string.IsNullOrEmpty(msg)) ViewBag.Msg = msg;
            if (string.IsNullOrEmpty(index)) return RedirectToAction("index", "home");
            var idOb = ParseUrl(index);
            var id = idOb["id"];
            var listing = db.ServiceListings.Find(int.Parse(id));
            if (listing == null) return RedirectToAction("index", "home");
            listing.Reviews = listing.Reviews == null ? new List<Review>() : listing.Reviews;
            var svo = "";
            listing.ServiceIds.ToList().ForEach(a =>
            {
                svo += db.Services.Find( int.Parse(a))?.Title + ", ";
            });
            var vm = new ServiceViewModel { Listing = listing, ServicesOffered = svo.Substring(0, svo.LastIndexOf(',')), TotalRating = GetRating(listing.Reviews.Sum(a => a.Rating), listing.Reviews.Count) };
            return View(vm);
        }


        private int  GetTypeId( string type)
        {
            if (type.ToLower() == "all") return -1;
            var cate = db.Services.Where(a => a.Title  == type).FirstOrDefault();
            if (cate == null) throw new ArgumentException("type dosent exist");
            return cate.Id;
        }


        //Insert a review!
        [Route("service/{index}"), HttpPost]
        public ActionResult Index(string index, Review review)
        {
            review.Date = DateTime.Now;
            review.AuthorId = User.Identity.GetUserId();
            review.Name = User.Identity.GetUser()?.Fullname ?? "Anonymous";
            if (string.IsNullOrEmpty(index)) return RedirectToAction("index", "home");
            var idOb = ParseUrl(index);
            var id = idOb["id"];
            var listing = db.ServiceListings.Find(int.Parse(id));
            if (listing == null) return Redirect($"Service/{index}");
            else
            {
                if (listing.Reviews != null)
                    listing.Reviews?.Add(review);
                else listing.Reviews = new List<Review> { review };
                db.Entry(listing).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return Redirect($"/Service/{index}?msg=Review Added");

        }

        [Route("service/add")]
        public ActionResult Add()
        {
            return View();
        }

        [Route("service/edit/{index}")]
        public ActionResult Edit(string index)
        {
            ViewBag.Id = index;
            return View();
        }

        [Route("service/Delete/{id}")]
        public ActionResult Delete(int id)
        {
            var list = db.ServiceListings.Find(id);
            if (list == null) return Redirect("services/all");
            if((User?.Identity.GetUserId() != list.AuthorId) && (!User.IsInRole("admin")) && (!User.IsInRole("moderator"))) Redirect("services/all"); 
            db.DeleteAll(list);
            db.Entry(list).State = System.Data.Entity.EntityState.Deleted;
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }

        [Route("service/RemoveReview")]
        public ActionResult RemoveReview(string index, int revId)
        {

            if (string.IsNullOrEmpty(index)) return RedirectToAction("index", "home");
            var idOb = ParseUrl(index);
            var id = idOb["id"];
            var listing = db.ServiceListings.Find(int.Parse(id));
            if (listing == null) return Redirect($"Service/{index}");
            if (!(User.IsInRole("admin") || User.IsInRole("moderator") || listing.AuthorId == User.Identity.GetUserId())) return Redirect($"/Service/{index}");
            try
            {
                listing.Reviews?.RemoveAt(revId);
                db.Entry(listing).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch
            {
            }
            return Redirect($"/Service/{index}");
        }

        private int GetRating(int total, int count)
        {
            if (total == 0) return 0;
            return (total / count);

        }
        private Dictionary<string, string> ParseUrl(string url)
        {
            var parts = url.Split('-');
            if (parts.Count() == 1) return new Dictionary<string, string>() { ["id"] = string.Empty, ["title"] = string.Empty };
            var dic = new Dictionary<string, string>();
            dic.Add("id", parts[0]);
            var title = "";
            for (int i = 1; i < parts.Count(); i++)
            {
                title += parts[i] + " ";
            }
            dic.Add("title", title);
            return dic;
        }


        [Route("Transaction/BeginVerify/{id}")]
        public async Task<ActionResult> BeginVerify(int id)
        {
         
           var listing = await db.ServiceListings.FindAsync(id);
            if (listing == null) return Redirect("services/all");
            var api = new PayStackApi(ConfigurationManager.AppSettings["payStackApiKey"]);
            var transaction = api.Transactions.Initialize(new TransactionInitializeRequest() { AmountInKobo = int.Parse(ConfigurationManager.AppSettings["verificationAmount"]), CallbackUrl = $"{CurrentUrl()}Transaction/FinishVerify", Email = User?.Identity.Name, MetadataObject = new Dictionary<string, object> { ["listing_id"] = listing.Id }, CustomFields = new List<CustomField>() { CustomField.From("Payment Type", "payment_id", "Payment for verififcation") } });
            if (transaction.Status)
            {
                return Redirect(transaction.Data.AuthorizationUrl);
            }
            return Redirect($"service/{listing.Id}");
        }



        private string CurrentUrl()
        {
           var stq = Request.Url.PathAndQuery;
            return Request.Url.AbsoluteUri.Replace(stq, "/");
        }

        [Route("Transaction/FinishVerify")]
        public ActionResult FinishVerify(string trxref)
        {
            try
            {
                var api = new PayStackApi(ConfigurationManager.AppSettings["payStackApiKey"]);
                var response = api.Transactions.Verify(trxref);
                if (response.Status && response.Data?.Status.ToLower() == "success")
                {
                    var id = Convert.ToInt32((response.Data.Metadata["listing_id"]));
                    var item = db.ServiceListings.Find(id);
                    if (item != null)
                    {
                        item.IsVerified = true;
                        //db.UpdateAll(item, null);
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return Redirect($"/service/{item.Id}-{item.Title.Replace(' ', '-')}?msg=You have been verified!");
                    }

                }
            }
            catch 
            {
            }
            
            
            return Content("Verification Failed");
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}