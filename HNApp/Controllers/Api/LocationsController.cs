﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HNApp.Models;
using System.Configuration;
using System.Web.Http.OData;
using System.Web.Http.Description;
using HNApp.Utils;
using System.Data.Entity;

namespace HNApp.Controllers.Api
{
   
    public class LocationsController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// All system location(Odata Querayable)
        /// </summary>
        /// <returns>array of locations</returns>
        [EnableQuery, ResponseType(typeof(ICollection<BaseLocation>))]
        public IQueryable<BaseLocation> Get()
        {
            return db.BaseLocations;
        }


        /// <summary>
        /// Gets all states in nigeria
        /// </summary>
        /// <returns></returns>
        [Route("api/locations/states"),EnableQuery,ResponseType(typeof(ICollection<string>))]
        public IHttpActionResult GetStates()
        {
            return Ok(db.BaseLocations.Select(a=>a.State).OrderBy(a => a));
        }


        /// <summary>
        /// Get all cities and local goverments in a state
        /// </summary>
        /// <param name="state">the name of the state</param>
        /// <returns></returns>
        [Route("api/locations/{state}/citlgas"), EnableQuery, ResponseType(typeof(ICollection<string>))]
        public IHttpActionResult GetCitiesLgas(string state)
        {
            var dstate = db.BaseLocations.Where(a => a.State.ToLower() == state.ToLower()).FirstOrDefault();
            if (dstate == null) return NotFound();

            return Ok(dstate.CityLga.Select(a => a.Name));
        }


        /// <summary>
        /// Get all areas in a city or lga
        /// </summary>
        /// <param name="state">the name of the state</param>
        /// <param name="cityLga">the name of the city or lga</param>
        /// <returns></returns>
        [Route("api/locations/{state}/citlgas/{cityLga}/areas"), EnableQuery, ResponseType(typeof(ICollection<string>))]
        public IHttpActionResult GetAreas(string state,string cityLga)
        {
            var dstate = db.BaseLocations.Where(a => a.State.ToLower() == state.ToLower()).FirstOrDefault();
            var lga = dstate?.CityLga?.Where(a => a.Name.ToLower() == cityLga.ToLower()).FirstOrDefault();
            if (lga == null) return NotFound();

            return Ok(lga.Area.OrderBy(a => a.Value));
        }



        /// <summary>
        /// Gets a single baselocation by id
        /// </summary>
        /// <param name="id">The location id</param>
        /// <returns></returns>\
        [ResponseType(typeof(BaseLocation))]
        public IHttpActionResult Get(int id)
        {
            var location = db.BaseLocations.Find(id);
            return Ok(location);
        }

        /// <summary>
        /// Adds a single location
        /// </summary>
        /// <param name="value">The location object</param>
        /// <returns></returns>
        [ApiAuthorize(Roles = "admin")]
        public IHttpActionResult Post(BaseLocation value)
        {
            if (value == null) return BadRequest();
            db.BaseLocations.Add(value);
            db.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Updates a location
        /// </summary>
        /// <param name="id">The locations's id</param>
        /// <param name="value">The location</param>
        /// <returns></returns>
        [ApiAuthorize(Roles = "admin")]
        public IHttpActionResult Put(int id, [FromBody]BaseLocation value)
        {
            var bl = db.BaseLocations.Find(id);
            if (value == null || bl == null) return BadRequest();
            db.Entry(value).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }


        /// <summary>
        /// Removes a location
        /// </summary>
        /// <param name="id">The location id</param>
        /// <returns></returns>
        [ApiAuthorize(Roles = "admin")]
        public IHttpActionResult Delete(int id)
        {

            var bl = db.BaseLocations.Find(id);
            if (bl == null) return BadRequest();

            db.DeleteAll(bl);
            db.Entry(bl).State = EntityState.Deleted;
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
