﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HNApp.Models;
using System.Configuration;
using System.Web.Http.OData;
using System.Web.Http.Description;
using System.Data.Entity;
using HNApp.Utils;
using Microsoft.AspNet.Identity;

namespace HNApp.Controllers.Api
{
    public class ServiceListingsController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// Gets all service listings
        /// </summary>
        /// <returns>The service listings</returns>
        [EnableQuery,ResponseType(typeof(ICollection<ServiceListing>))]
        public IQueryable<ServiceListing> Get()
        {
            return db.ServiceListings;
        }
        

        /// <summary>
        /// Gets a single service listing
        /// </summary>
        /// <param name="id">The service listing id</param>
        /// <returns></returns>
        [EnableQuery, ResponseType(typeof(ServiceListing))]
        public IHttpActionResult Get(int id)
        {
            var result = db.ServiceListings.Find(id);
            return Ok(result);
        }

        /// <summary>
        /// Get the average rating interms if 5 points of a service listing
        /// </summary>
        /// <param name="id">The service listing id</param>
        /// <returns>a 5 point average of a listing</returns>
        [HttpGet, Route("api/ServiceListings/{id}/rating"), ResponseType(typeof(int))]
        public IHttpActionResult GetRating(int id)
        {
            var reviews = db.ServiceListings.Find(id)?.Reviews;
            if (reviews == null) return Ok(0);
            var rsum = (reviews.Sum(a => a.Rating) / reviews.Count);
            return Ok(rsum);
        }
        /// <summary>
        /// Gets all reviews of a service listing
        /// </summary>
        /// <param name="id">The serivce listing id</param>
        /// <returns></returns>
        [HttpGet,EnableQuery,Route("api/ServiceListings/{id}/reviews")]
        public IQueryable<Review> GetReviews(int id)
        {
            var reviews = db.ServiceListings.Find(id).Reviews;

            return reviews.AsQueryable();
        }

        /// <summary>
        /// Adds a review to a service listing
        /// </summary>
        /// <param name="id">The servicelisting id</param>
        /// <param name="review">The eview object</param>
        /// <returns></returns>
        [ApiAuthorize(Roles = "admin")]
        [HttpPost, Route("api/ServiceListings/{id}/reviews"), ResponseType(typeof(Review))]
        public IHttpActionResult PostReview(int id, Review review)
        {
            var sv = db.ServiceListings.Find(id);

            sv.Reviews.Add(review);
            db.Entry(sv).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok(review);
        }


        /// <summary>
        /// removes a review from a service listing
        /// </summary>
        /// <param name="id">The service listing id</param>
        /// <param name="sid">The review id</param>
        /// <returns></returns>
        [ApiAuthorize]
        [HttpDelete, Route("api/ServiceListings/{id}/reviews/{sid}")]
        public IHttpActionResult DeleteReview(int id, int sid)
        {

            var list = db.ServiceListings.Find(id);
            var uid = User?.Identity.GetUserId();
            if (!string.IsNullOrEmpty(uid))
            {
                if ((uid != list.Reviews[sid].AuthorId) || !User.IsInRole("admin") || !User.IsInRole("moderator")) return StatusCode(HttpStatusCode.Forbidden);
            }
            list.Reviews.RemoveAt(sid);
            db.Entry(list).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }


        /// <summary>
        /// Adds a new service listing
        /// </summary>
        /// <param name="value">The service listing object</param>
        /// <returns>The inserted service listing</returns>
        [ApiAuthorize]
        [ResponseType(typeof(ServiceListing))]
        public IHttpActionResult Post([FromBody]ServiceListing value)
        {
            value.Date = DateTime.Now;
            if (value == null) return BadRequest();
            db.ServiceListings.Add(value);
            db.SaveChanges();
            return Ok(value);
        }

        /// <summary>
        /// Updates a service listing
        /// </summary>
        /// <param name="id">The service listing id</param>
        /// <param name="value">The service listing object</param>
        /// <returns>The updated service</returns>
        [ApiAuthorize]
        [ResponseType(typeof(ServiceListing))]
        public IHttpActionResult Put(int id, [FromBody]ServiceListing value)
        {
            if (id != value.Id) return BadRequest();
            var uid = User?.Identity.GetUserId();
            if (!string.IsNullOrEmpty(uid))
            {
                if ((uid != value.AuthorId) && !User.IsInRole("admin") && !User.IsInRole("moderator")) return StatusCode(HttpStatusCode.Forbidden);
            }
            ServiceListing sod = null;
            using (ApplicationDbContext app = new ApplicationDbContext())
            {
                var sd = app.ServiceListings.Include(a => a.Reviews).Include(a => a.Contact).Include(a => a.Location).Single(a => a.Id == id);
                sod = sd.DeepCopy();
            }

            sod.ServiceIds?.ForEach(a =>
            {
                if (value.ServiceIds?.Exists(b => (b.Value == a.Value) && (b.Id == 0)) == true)
                {
                    value.ServiceIds.RemoveAt(value.ServiceIds.FindIndex(c => c.Value == a.Value));
                    value.ServiceIds.Add(a);
                }
            });

            db.UpdateAll(value, sod);
            db.Entry(value).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Removes a service listinf
        /// </summary>
        /// <param name="id">The service listing id</param>
        /// <returns></returns>
        [ApiAuthorize]
        public IHttpActionResult Delete(int id)
        {
            var list = db.ServiceListings.Find(id);
            var uid = User?.Identity.GetUserId();
            if (!string.IsNullOrEmpty(uid))
            {
                if ((uid != list.AuthorId) && !User.IsInRole("admin") && !User.IsInRole("moderator")) return StatusCode(HttpStatusCode.Forbidden);
            }
            if (list == null) return BadRequest();

            db.DeleteAll(list);
            db.Entry(list).State = EntityState.Deleted;
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
