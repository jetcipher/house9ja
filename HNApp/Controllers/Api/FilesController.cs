﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.Web;
using System.IO;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace HNApp.Controllers.Api
{
    public class FilesController : ApiController
    {
        
        /// <summary>
        /// Uploads a single file
        /// </summary>
        /// <returns>a file url</returns>
        [ResponseType(typeof(string))]
        public IHttpActionResult Post()
        {
            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
            //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            //CloudBlobContainer container = blobClient.GetContainerReference("files");
            try
            {
                //HttpFileCollection files = HttpContext.Current.Request.Files;
                //int fileCount = files.Count;
                var file = "";
                //if (fileCount > 0)
                //{
                //    for (int i = 0; i < fileCount; i++)
                //    {
                //        var path = GetRandomBlobName(files[i].FileName);
                //        file = $"https://hosenaija.blob.core.windows.net/files/{path}";
                //        CloudBlockBlob blob = container.GetBlockBlobReference(path);
                //        byte[] data;
                //        using (System.IO.MemoryStream ms = new MemoryStream())
                //        {
                //            files[i].InputStream.CopyTo(ms);
                //            data = ms.ToArray();
                //        }
                //       Task.Run(()=> blob.UploadFromByteArray(data,0,data.Length));
                //    }
                //}

                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    foreach (string item in HttpContext.Current.Request.Files)
                    {
                        file = TOFileSystem(HttpContext.Current.Request.Files[item]);
                    }
                }
                return Ok(CurrentUrl()+file);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        private string CurrentUrl()
        {
            var stq = HttpContext.Current.Request.Url.PathAndQuery;
            return HttpContext.Current.Request.Url.AbsoluteUri.Replace(stq, "");
        }


        private string TOFileSystem(HttpPostedFile file)
        {
            if (file.ContentLength <= 0) return string.Empty;
            else
            {
                
                var okFilename = GetRandomBlobName(file.FileName);
                var dirpath = System.Web.HttpContext.Current.Server.MapPath($"~/ListingFiles/");
                if (!System.IO.Directory.Exists(dirpath)) System.IO.Directory.CreateDirectory(dirpath);
                var filePath = System.Web.HttpContext.Current.Server.MapPath($"~/ListingFiles/{okFilename}");
                file.SaveAs(filePath);
                return $"/ListingFiles/{okFilename}";
            }

        }


        private string GetRandomBlobName(string filename)
        {
            string ext = Path.GetExtension(filename);
            return string.Format("{0:10}_{1}{2}", DateTime.Now.Ticks, Guid.NewGuid(), ext);
        }




        /// <summary>
        /// Deletes a file from the server
        /// </summary>
        /// <param name="path">The url of the file</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Delete(string path)
        {
            try
            {
                
                var filepath = System.Web.HttpContext.Current.Server.MapPath(path);
                File.Delete(filepath);
                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //CloudBlobContainer container = blobClient.GetContainerReference("files");
                //container.CreateIfNotExists(BlobContainerPublicAccessType.Blob);
                //Uri uri = new Uri(path.Replace("\"",""));
                //string filename = Path.GetFileName(uri.LocalPath);

                //var blob = container.GetBlockBlobReference(filename);
                //await blob.DeleteIfExistsAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
