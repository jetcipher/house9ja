﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HNApp.Models;
using System.Configuration;
using System.Web.Http.OData;
using System.Web.Http.Description;
using HNApp.Utils;
using System.Data.Entity.Migrations;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace HNApp.Controllers.Api
{
    [ApiAuthorize]
    public class PropertyListingsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private bool PropertyListingExists(int key)
        {
            return db.PropertyListings.Find(key) != null;
        }

        /// <summary>
        /// Gets all property listings (OData Querayable)
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous,EnableQuery]
        public IQueryable<PropertyListing> Get()
        {
            return db.PropertyListings;
        }

       /// <summary>
       /// Get a single property listing
       /// </summary>
       /// <param name="id">The listing id</param>
       /// <returns></returns>
      [AllowAnonymous,ResponseType(typeof(PropertyListing))]
        public IHttpActionResult Get(int id)
        {
            var result = db.PropertyListings.Find(id);
            return Ok(result);
        }

        /// <summary>
        /// Adds a property listing
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [ResponseType(typeof(PropertyListing))]
        public IHttpActionResult Post(PropertyListing value)
        {
            value.Date = DateTime.Now;
            if (value == null) return BadRequest();
            db.PropertyListings.Add(value);
            db.SaveChanges();
            return Ok(value);
        }

        /// <summary>
        /// Udates a property listing
        /// </summary>
        /// <param name="id">The property id</param>
        /// <param name="value">The proerty object</param>
        /// <returns>The updated property</returns>
        [ ResponseType(typeof(PropertyListing))]
        public IHttpActionResult Put(int id, [FromBody]PropertyListing value)
        {
            

            if (id != value.Id) return BadRequest();
            var uid = User?.Identity.GetUserId();
            if(!string.IsNullOrEmpty(uid))
            {
                if ((uid != value.AuthorId) && !User.IsInRole("admin") && !User.IsInRole("moderator")) return StatusCode(HttpStatusCode.Forbidden);
            }


            PropertyListing nod = null;
            using (ApplicationDbContext app = new ApplicationDbContext())
            {
                var od = app.PropertyListings.Include(a => a.Descriptions).Include(a => a.Contact).Include(a => a.Location).Include(a => a.Photos).Single(a => a.Id == id);
                 nod = od.DeepCopy();
            }
            nod.Photos?.ForEach(a =>
            {
                if (value.Photos?.Exists(b => (b.Value == a.Value) && (b.Id == 0)) == true)
                {
                    value.Photos.RemoveAt(value.Photos.FindIndex(c => c.Value == a.Value));
                    value.Photos.Add(a);
                }
            });

            db.UpdateAll(value,nod);
             db.Entry(value).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {

                //throw;
            }
            
            return Ok();
        }


        /// <summary>
        /// Delets a property
        /// </summary>
        /// <param name="id">The property id</param>
        /// <returns></returns>
        public IHttpActionResult Delete(int id)
        {
            var pl = db.PropertyListings.Find(id);
            if (pl == null) return BadRequest();
            var uid = User?.Identity.GetUserId();
            if (!string.IsNullOrEmpty(uid))
            {
                if ((uid != pl.AuthorId) && !User.IsInRole("admin") && !User.IsInRole("moderator")) return StatusCode(HttpStatusCode.Forbidden);
            }
            db.DeleteAll(pl);
            db.Entry(pl).State = EntityState.Deleted;
            db.PropertyListings.Remove(pl);
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
