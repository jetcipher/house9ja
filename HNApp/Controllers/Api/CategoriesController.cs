﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HNApp.Models;
using System.Configuration;
using System.Web.Http.OData;
using System.Web.Http.Description;
using System.Data.Entity;
using HNApp.Utils;

namespace HNApp.Controllers.Api
{
    [ApiAuthorize]
    public class CategoriesController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();
       

        /// <summary>
        /// Gets all categories e.g House,Lands etc (OData Querayable)
        /// </summary>
        /// <returns>An Application user object, refer to class diagram</returns>
        /// <response code="200">Array of all categories</response>
        [AllowAnonymous,EnableQuery]
        public IQueryable<Categorey> Get()
        {
            return db.Categories;
        }

        /// <summary>
        /// Get a single categorey
        /// </summary>
        /// <param name="id">The string id of the categorey in the url</param>
        /// <returns>a single categorey</returns>
        /// <response code="200">Categorey object</response>
        [AllowAnonymous,ResponseType(typeof(Categorey))]
        public IHttpActionResult Get(int id)
        {
            var cat = db.Categories.Find(id);
            return Ok(cat);
        }

        /// <summary>
        /// Gets an array of all types under a categorey for example a House might have two bedroom etc..
        /// </summary>
        /// <param name="id">The categorey's string ID</param>
        /// <returns>An Application user object, refer to class diagram</returns>
        /// <response code="200">string array of all types</response>
        /// <response code="400">Bad request</response>
        [AllowAnonymous, Route("api/Categories/{id}/types"), ResponseType(typeof(ICollection<string>))]
        public IHttpActionResult GetTypes(int id)
        {
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();
            return Ok(cat.Types);
        }


        /// <summary>
        /// Get all properties under a categorey (OData Querayable)
        /// </summary>
        /// <param name="id">The categorey's ID</param>
        /// <returns>Array of Property Listings(Refer to class diagram)</returns>
        /// <response code="200">Array of Property Listings(Refer to class diagram)</response>
        [AllowAnonymous, EnableQuery,Route("api/Categories/{id}/properties")]
        public IQueryable<PropertyListing> GetProperties(int id)
        {
            return db.PropertyListings.Where(a=>a.CategoreyId == id);
        }


        /// <summary>
        /// Returns all property listings under a type,under a categorey(OData Querayable)
        /// </summary>
        /// <param name="id">The string id of the categorey</param>
        /// <param name="typeId">The integer id of the type</param>
        /// <returns>an array of properties(Refer to class diagram)</returns>
        [AllowAnonymous, EnableQuery,Route("api/Categories/{id}/types/{typeId}/properties")]
        public IQueryable<PropertyListing> GetTypeProperties(int id, int typeId)
        {
            
            return db.PropertyListings.Where(a => a.CategoreyId == id && a.TypeId == typeId);
        }


        /// <summary>
        /// Gets all allowed description for a categorey eg a house can have descriptions like No of bedrooms,toilets etc
        /// </summary>
        /// <param name="id">The categorey id</param>
        /// <returns>an array of string</returns>
        [AllowAnonymous, Route("api/Categories/{id}/Descriptions"), ResponseType(typeof(ICollection<string>))]
        public IHttpActionResult GetDescriptions(int id)
        {
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();
            return Ok(cat.AllowedDescriptions);
        }


        /// <summary>
        /// Gets a single type under a categorey
        /// </summary>
        /// <param name="id">Categorey Id</param>
        /// <param name="typeId">The type id</param>
        /// <returns>A string name of the categorey</returns>
        [AllowAnonymous, Route("api/Categories/{id}/types/{typeId}"), ResponseType(typeof(string))]
        public IHttpActionResult GetType(int id,int typeId)
        {
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();
            return Ok(cat.Types[typeId]);
        }
        
        /// <summary>
        /// Adds a single type to a categorey
        /// </summary>
        /// <param name="id">the categorey id</param>
        /// <param name="type">The type string</param>
        /// <returns>The type string</returns>
        [Route("api/Categories/{id}/types/"), ResponseType(typeof(string))]
        public IHttpActionResult PostType(int id, string type)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();

            cat.Types.Add(type);
            db.Entry(cat).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok(type);
        }

        /// <summary>
        /// Adds an allowed description to this categorey
        /// </summary>
        /// <param name="id">the category's id</param>
        /// <param name="desc">the description name</param>
        /// <returns></returns>

        [Route("api/Categories/{id}/Descriptions/"),ResponseType(typeof(string))]
        public IHttpActionResult PostDescription(int id,string desc)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();

            cat.AllowedDescriptions.Add(desc);
            db.Entry(cat).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok(desc);
        }


        /// <summary>
        /// Add a new categorey
        /// </summary>
        /// <param name="categorey">The categorey object</param>
        /// <returns>The inserte categorey</returns>
        [ResponseType(typeof(Categorey))]
        public IHttpActionResult Post(Categorey categorey)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            if (categorey == null) return BadRequest();
            categorey.Date = DateTime.Now;
            db.Categories.Add(categorey);
            db.SaveChanges();
            return Ok(categorey);
        }

        /// <summary>
        /// Updates a categorey
        /// </summary>
        /// <param name="id">the categorey id</param>
        /// <param name="categorey">The categorey object</param>
        /// <returns>The categorey object</returns>
        [ResponseType(typeof(Categorey))]
        public IHttpActionResult Put(int id, Categorey categorey)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            if (id != categorey.Id) return BadRequest();
            Categorey nod = null;
            using (ApplicationDbContext app = new ApplicationDbContext())
            {
                var od = app.Categories.Include(a => a.AllowedDescriptions).Include(a => a.Types).Single(a => a.Id == id);
                nod = od.DeepCopy();
            }
            nod.AllowedDescriptions?.ForEach(a =>
            {
                if (categorey.AllowedDescriptions?.Exists(b => (b.Value == a.Value) && (b.Id == 0)) == true)
                {
                    categorey.AllowedDescriptions.RemoveAt(categorey.AllowedDescriptions.FindIndex(c => c.Value == a.Value));
                    categorey.AllowedDescriptions.Add(a);
                }
            });

            nod.Types?.ForEach(a =>
            {
                if (categorey.Types?.Exists(b => (b.Value == a.Value) && (b.Id == 0)) == true)
                {
                    categorey.Types.RemoveAt(categorey.Types.FindIndex(c => c.Value == a.Value));
                    categorey.Types.Add(a);
                }
            });


            db.UpdateAll(categorey, nod);
            db.Entry(categorey).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {

                //throw;
            }

            return Ok(categorey);
        }

       /// <summary>
       /// Updates a type under a categorey
       /// </summary>
       /// <param name="id">The categorey id</param>
       /// <param name="typeId">The type index</param>
       /// <param name="type">The string type</param>
       /// <returns></returns>
        [Route("api/Categories/{id}/types/{typeId}")]
        public IHttpActionResult PutType(int id, int typeId, string type)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();

            cat.Types[typeId].Value = type;
            db.Entry(cat).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return Ok();
        }

        /// <summary>
        /// Updates a type under a categorey
        /// </summary>
        /// <param name="id">The categorey id</param>
        /// <param name="descriptionId">The type index</param>
        /// <param name="description">The string type</param>
        /// <returns></returns>
        [Route("api/Categories/{id}/Descriptions/{descriptionId}")]
        public IHttpActionResult PutDescription(int id, int descriptionId, string description)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();

            cat.AllowedDescriptions[descriptionId].Value = description;
            db.Entry(cat).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return Ok();
        }



        /// <summary>
        /// Deletes a categorey
        /// </summary>
        /// <param name="id">The categorey Id</param>
        /// <returns></returns>
        public IHttpActionResult Delete(int id)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();
            db.DeleteAll(cat);
            db.Entry(cat).State = EntityState.Deleted;
            db.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Removes a type under a categorey
        /// </summary>
        /// <param name="id">The categorey Id</param>
        /// <param name="typeId">The types index</param>
        /// <returns></returns>
        [Route("api/Categories/{id}/types/{typeId}")]
        public IHttpActionResult DeleteType(int id, int typeId)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();
            cat.Types.RemoveAt(typeId);
            db.Entry(cat).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }


        /// <summary>
        /// Removes a description from a categorey
        /// </summary>
        /// <param name="id">The categorey Id</param>
        /// <param name="Did">The description id</param>
        /// <returns></returns>
        [Route("api/Categories/{id}/Descriptions/{Did}")]
        public IHttpActionResult DeletetDescription(int id,int Did)
        {
            if (User?.IsInRole("admin") != true) return StatusCode(HttpStatusCode.Forbidden);
            var cat = db.Categories.Find(id);
            if (cat == null) return BadRequest();
            cat.AllowedDescriptions.RemoveAt(Did);
            db.Entry(cat).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
