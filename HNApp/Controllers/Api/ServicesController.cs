﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HNApp.Models;
using System.Configuration;
using System.Web.Http.OData;
using System.Web.Http.Description;
using HNApp.Utils;
using System.Web.Mvc.Html;
using System.Data.Entity;

namespace HNApp.Controllers.Api
{
    public class ServicesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public IQueryable<Service> Get()
        {
            return db.Services;
        }


        /// <summary>
        /// Get all service listings under a service listing type (OData Querayable)
        /// </summary>
        /// <param name="id">The service id</param>
        /// <returns></returns>
        [EnableQuery, Route("api/services/{id}/Listings")]
        public IQueryable<ServiceListing> GetServiceListings(int id)
        {
            return db.ServiceListings.Where(a => a.ServiceIds.Contains(id.ToString()));
        }


        /// <summary>
        /// Get a single service type
        /// </summary>
        /// <param name="id">The service type id</param>
        /// <returns></returns>
        [ResponseType(typeof(Service))]
        public IHttpActionResult Get(int id)
        {
            var serv = db.Services.Find(id);
            return Ok(serv);
        }

        /// <summary>
        /// Adds a single service
        /// </summary>
        /// <param name="value">The service object</param>
        /// <returns>The inserted object</returns>
        [ApiAuthorize(Roles = "admin")]
        public IHttpActionResult Post(Service value)
        {
            if (value == null) return BadRequest();
            value.Date = DateTime.Now;
            db.Services.Add(value);
            db.SaveChanges();
            return Ok(value);
        }

        /// <summary>
        /// Updates a service type by id
        /// </summary>
        /// <param name="id">The service tye id</param>
        /// <param name="value">The service object</param>
        /// <returns>The udated service</returns>
        [ApiAuthorize(Roles = "admin")]
        [ResponseType(typeof(Service))]
        public IHttpActionResult Put(int id, [FromBody]Service value)
        {
            
            var list = db.Services.Find(id);
            if (list == null || value == null) return BadRequest();

            db.Update(list, value);
            return Ok(value);
        }


        /// <summary>
        /// removes a single service type by id
        /// </summary>
        /// <param name="id">The service id</param>
        /// <returns></returns>
        [ApiAuthorize(Roles = "admin")]
        public IHttpActionResult Delete(int id)
        {

            var list = db.Services.Find(id);
            if (list == null) return BadRequest();

            db.DeleteAll(list);
            db.Entry(list).State = EntityState.Deleted;
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
