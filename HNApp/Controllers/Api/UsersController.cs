﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HNApp.Models;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace HNApp.Controllers.Api
{
    public class UsersController : ApiController
    {
        public ApplicationUserManager UserManager { get {

               return HttpContext.Current.GetOwinContext().Get<ApplicationUserManager>();
            }
        }


        /// <summary>
        /// Get a user by email, usefull for login from facebook and google
        /// </summary>
        /// <param name="email">The users email address</param>
        /// <returns>An Application user object, refer to class diagram</returns>
        /// <response code="200">Application User object</response>
        /// <response code="404">Not found</response>
        [Route("api/users/{email}")]
        public async Task<IHttpActionResult> Get(string email)
        {

            if (string.IsNullOrEmpty(email)) return BadRequest("Email Address cannot be null");
            var user = await UserExists(email);
            if (user == null) return NotFound();

            return Ok(user);
        }


        [Route("api/users"),EnableQuery,HttpGet]
        public IQueryable<ApplicationUser> Get()
        {
            return UserManager.Users;
        }

        private async Task<ApplicationUser> UserExists(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);
            return user;
        }

        /// <summary>
        /// Gets a user by email and password
        /// </summary>
        /// <param name="email">The users email</param>
        /// <param name="password">The users password</param>
        /// <response code="200">Application User object</response>
        /// <response code="404">Not found</response>

        [Route("api/users/{email}/{password}"), ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> Get(string email,string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password)) return BadRequest("Email Address and or password cannot be null");
            var user = await UserExists(email);
            if (user == null) return NotFound();

            if(UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash,password) != Microsoft.AspNet.Identity.PasswordVerificationResult.Success) return StatusCode(HttpStatusCode.Unauthorized);
            return Ok(user);
        }

        /// <summary>
        /// Adds a user. The passwors is optional, only for password sign up
        /// </summary>
        /// <param name="user">The user object</param>
        /// <param name="password">Password(optional)</param>
        /// <response code="200">Application User object</response>
        /// <response code="400">Bad request for invalid user object</response>
        [Route("api/users/{password}"), ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> Post(ApplicationUser user,string password = null)
        {
            if (user == null) return BadRequest();
            if (!ModelState.IsValid)
            {
                return BadRequest("Make sure all fields are provided.");
            }

            if(string.IsNullOrEmpty(password))
            {
                var result = await UserManager.CreateAsync(user);
                if(!result.Succeeded) return BadRequest(result.Errors.FirstOrDefault());
            }
            else
            {
                var result = await UserManager.CreateAsync(user, password);
                if (!result.Succeeded) return BadRequest(result.Errors.FirstOrDefault());
            }
            return Ok(user);
        }

        /// <summary>
        /// Updates a user
        /// </summary>
        /// <param name="user">The user object</param>
        /// <returns>The udated user</returns>
        [ApiAuthorize,Route("api/users"), HttpPut,ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> Put( ApplicationUser user)
        {
            if (user == null) return BadRequest();

            var result = await UserManager.UpdateAsync(user);

            return Ok(user);
        }

        /// <summary>
        /// Updates a user
        /// </summary>
        /// <param name="id">The users unique user</param>
        /// <returns>200 for ok, and 400 for bad request</returns>
        [ApiAuthorize(Roles ="admin") ,Route("api/users/{id}"), HttpDelete]
        public async Task<IHttpActionResult> Delete(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null) return BadRequest();
           var result=  await UserManager.DeleteAsync(user);
            if (result.Succeeded) return Ok();
            else return BadRequest();
        }


    }
}
