﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;
using System.Data.Entity;
using System.Reflection;
using System.Collections;

namespace HNApp.Utils
{
    public static class Helpers
    {
        public static Dictionary<string, int> ExplodeEnum(Type obj)
        {

            Dictionary<string, int> val = new Dictionary<string, int>();
            string[] names = obj.GetEnumNames();
            for (int i = 0; i < names.Length; i++)
            {
                val.Add(names[i], (int)Enum.Parse(obj, names[i]));
            }
            return val;
        }


        public static string GetMailStr(string msg, string name)
        {
            string body = "";
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Mail/Mailer.html")))
            {
                body = reader.ReadToEnd();
            }
            return body.Replace("{message}", msg).Replace("{name}", name);
        }


        public static Dictionary<int,string> GetEnumFlagValues<T>(int currentFlag) 
        {
            
            var dic = new Dictionary<int, string>();
            var values = typeof(T).GetEnumValues();
            foreach (int item in values)
            {
                if ((currentFlag & item) == item)
                {
                    dic.Add(item, typeof(T).GetEnumName(item));
                }
            }
            return dic;
        }


        public static void Update<T>(this DbContext parent, T oldValue, T newValue) where T : class
        {


            

            parent.Entry(oldValue).CurrentValues.SetValues(newValue);
            parent.SaveChanges();
        }



        public static void UpdateAll(this DbContext context,object parent,object oldParent)
        {
            
            var properties = parent.GetType().GetProperties().Where(a => (a.PropertyType.IsClass) && (a.PropertyType.Name != typeof(string).Name));
            foreach (var item in properties)
            {
                if (!IsCollection(item))
                {
                   // var value = item.GetValue(parent,null);
                    if(typeof(Models.IKey).IsAssignableFrom(item.PropertyType))
                    {
                        Models.IKey value = item.GetValue(parent, null) as Models.IKey;
                        if (value != null)
                        {

                            if (value.Id == 0) context.Entry(value).State = EntityState.Added;
                            else context.Entry(value).State = EntityState.Modified;
                            context.UpdateAll(value, item.GetValue(oldParent, null));
                        }
                        else
                        {
                            var oov = item.GetValue(oldParent, null);
                            if (oov != null) context.Entry(oov).State = EntityState.Deleted;
                        }
                    }
                   
                    
                }
                else
                {
                    var val = item.GetValue(parent, null) as IList;
                    var p2 = item.GetValue(oldParent, null);

                    if(p2!= null)
                    {
                        try
                        {
                            var oval = p2 as IList;
                            if (val?.Count < oval.Count)
                            {

                                IEnumerable<Models.IKey> data1 = val.Cast<Models.IKey>();
                                IEnumerable<Models.IKey> odata = oval.Cast<Models.IKey>();
                                var badData = odata.Where(a => !data1.Any(b => b.Id == a.Id)).ToArray();
                                foreach (object obj in badData)
                                {
                                    context.Entry(obj).State = EntityState.Deleted;
                                }
                                //odata.Where(a => !data1.Any(b => b.Id == a.Id));
                            }
                        }

                        catch(Exception e)
                        {

                            throw new ArgumentException("Entites must implement Ikey Interface");
                        }
                    }
                    if (val != null)
                    {
                        foreach (var it in val)
                        {
                            if (it as Models.IKey != null)
                            {
                                Models.IKey listVal = it as Models.IKey;
                                if ((listVal != null) && (listVal.Id == 0)) context.Entry(listVal).State = EntityState.Added;
                                else if (listVal != null) context.Entry(listVal).State = EntityState.Modified;
                                context.UpdateAll(it, null);
                            }

                        }
                    }
                   
                    
                   
                }
            }
        }

        public static void DeleteAll(this DbContext context,object parent)
        {
            var properties = parent.GetType().GetProperties().Where(a => (a.PropertyType.IsClass) && (a.PropertyType.Name != typeof(string).Name));
            foreach (var item in properties)
            {
                if (!IsCollection(item))
                {
                    // var value = item.GetValue(parent,null);
                    if (typeof(Models.IKey).IsAssignableFrom(item.PropertyType))
                    {
                        var value = item.GetValue(parent, null);
                        if (value != null)
                        {

                            
                            context.DeleteAll(value);
                            context.Entry(value).State = EntityState.Deleted;
                        }
                    }


                }
                else
                {
                    var val = item.GetValue(parent, null) as IList;
                   
                    if (val != null)
                    {
                        for (int i = val.Count - 1; i >= 0; i--)
                        {
                            if (val[i] != null)
                            {

                                context.DeleteAll(val[i]);
                                context.Entry(val[i]).State = EntityState.Deleted;
                            }
                        }
                       
                    }



                }
            }
        }

        private static bool IsCollection(PropertyInfo pi)
        {
            return pi.PropertyType.FullName.Contains("System.Collections");
        }



        public static T DeepCopy<T>(this T obj) where T:class,new()
        { 
          return fastJSON.JSON.ToObject<T>(fastJSON.JSON.ToJSON(obj));
        }


        public static void MapTo<T>(this T source, T destination)
        {
            if (source == null || destination == null)
                throw new Exception("Source or/and Destination Objects are null");
            // Getting the Types of the objects
            Type typeDest = source.GetType();
            Type typeSrc = destination.GetType();
            // Collect all the valid properties to map
            var results = from srcProp in typeSrc.GetProperties()
                          let targetProperty = typeDest.GetProperty(srcProp.Name)
                          where srcProp.CanRead
                          && targetProperty != null
                          && (targetProperty.GetSetMethod(true) != null && !targetProperty.GetSetMethod(true).IsPrivate)
                          && (targetProperty.GetSetMethod().Attributes & MethodAttributes.Static) == 0
                          && targetProperty.PropertyType.IsAssignableFrom(srcProp.PropertyType)
                          select new { sourceProperty = srcProp, targetProperty = targetProperty };
            //map the properties
            foreach (var props in results)
            {
                props.targetProperty.SetValue(source, props.sourceProperty.GetValue(destination, null), null);
            }
        }



    }
}