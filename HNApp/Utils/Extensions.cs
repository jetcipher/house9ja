﻿using HNApp;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;



namespace System.Web.Mvc.Html
{ 
    public static class Helpers
    {
        public static MvcHtmlString Pager(this HtmlHelper helper,int totalCount,int pageSize,int pageNo,int pagesToDisplay)
        {

            var numPages = (pageSize >= totalCount) ? 1 : (int)Math.Ceiling(((decimal)totalCount) / ((decimal)pageSize));
            if (numPages == 1) return new MvcHtmlString("");
            var clist = new StringBuilder("<div class=\"pagination\"> <div class=\"center\"> <ul>");
            var start = 1;
            var end = numPages;
            clist.AppendLine((pageNo > 1) ?  AddPageLink(pageNo - 1, helper, displayText: "«") : "<li class=\"disabled\" disabled><a>«</a></li>");
            if (numPages > pagesToDisplay)
            {

                var middle = (int)Math.Ceiling(pagesToDisplay / 2d) - 1;
                var below = (pageNo - middle);
                var above = (pageNo + middle);

                if (below < 2)
                {
                    above = pagesToDisplay;
                    below = 1;
                }
                else if (above > (numPages - 2))
                {
                    above = numPages;
                    below = (numPages - pagesToDisplay + 1);
                }

                start = below;
                end = above;
            }


            if (start > 1)
            {
                clist.AppendLine(AddPageLink(1, helper));
                if (start > 3)
                {
                    clist.AppendLine(AddPageLink(2, helper));
                }
                if (start > 2)
                {
                    clist.AppendLine("<li class=\"disabled\" disabled><a>...</a></li>");
                }
            }

            for (var i = start; i <= end; i++)
            {
                if (i == pageNo || (pageNo <= 0 && i == 1))
                {
                    clist.AppendLine(AddPageLink(i, helper,true));
                }
                else
                {
                    clist.AppendLine(AddPageLink(i, helper));
                }
            }

            if (end < numPages)
            {
                if (end < numPages)
                {
                    clist.AppendLine("<li class=\"disabled\" disabled><a>...</a></li>");
                }
                if (end < numPages - 2)
                {
                    clist.AppendLine(AddPageLink(numPages, helper));
                }
            }

            clist.AppendLine((pageNo < numPages)?AddPageLink(pageNo + 1, helper,displayText: "»") : "<li class=\"disabled\" disabled>><a>»</a></li>");

            clist.AppendLine("</ul></div><div class=\"clear\"></div></div>");
            return new MvcHtmlString(clist.ToString());
        }

        public static MvcHtmlString RenderActive(this HtmlHelper helper, string html,string check)
        {
            if ((helper.ViewBag.Active != null) && (helper.ViewBag.Active == check)) return MvcHtmlString.Create(html);
            else return MvcHtmlString.Empty;
        }
        public static HNApp.Models.ApplicationUser GetUser(this IIdentity uid)
        {
            var id = uid.GetUserId();
           var userManager =  HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            return userManager.FindById(id);
        }


        public static MvcHtmlString ValidateMessage(this HtmlHelper helper)
        {
            var errors = GetModelStateList(helper);
            if (errors.Count() <= 0) return new MvcHtmlString("");

            return new MvcHtmlString($"<div class=\"alert-box error\" role=\"alert\"><em class=\"fa fa-lg fa-warning\">&nbsp;</em> {errors.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage}</div>");


        }

        private static IEnumerable<ModelState> GetModelStateList(HtmlHelper htmlHelper)
        {
           
                ModelState ms;
                htmlHelper.ViewData.ModelState.TryGetValue(htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix, out ms);
                if (ms != null)
                {
                    return new ModelState[] { ms };
                }

                return new ModelState[0];
        }
        private static string AddPageLink(int num,HtmlHelper helper,bool iscurrent = false, string displayText = "")
        {
            if (string.IsNullOrEmpty(displayText)) displayText = num.ToString();
            var url = helper.ViewContext.HttpContext.Request.RawUrl;
            var ct = ParseUrl(url);
            if (ct.Count >= 1)
            {
                if (ct.Keys.Count(a=>a == "pageNo") != 1) return $"<li {(iscurrent ? "class=\"current\"" : "")}><a class=\"button small grey\" href=\"{url}&pageNo={num}\">{displayText}</a></li>";
                else return $"<li {(iscurrent ? "class=\"current\"" : "")}><a class=\"button small grey\" href=\"{url.Replace(" ? pageNo="+ct["pageNo"], $"?pageNo={num}").Replace("&pageNo=" + ct["pageNo"], $"&pageNo={num}")}\">{displayText}</a></li>";
            }
            else
            {
                return $"<li {(iscurrent ? "class=\"current\"" : "")}><a class=\"button small grey\" href=\"{url}?pageNo={num}\">{displayText}</a></li>";
            }
            
        }

        public static string GetPagerUrl(this HtmlHelper helper)
        {
            var url = helper.ViewContext.HttpContext.Request.RawUrl;
            var ct = ParseUrl(url);
            var num = "%num%";
            if (ct.Count >= 1)
            {
                if (ct.Keys.Count(a => a == "pageNo") != 1) return $"{url}&pageNo={num}";
                else return $"{url.Replace("?pageNo=" + ct["pageNo"], $"?pageNo={num}").Replace("&pageNo=" + ct["pageNo"], $"&pageNo={num}")}";
            }
            else
            {
                return $"{url}?pageNo={num}";
            }
        }


        private static Dictionary<string,string> ParseUrl(string url)
        {
            var items = url.Split('?', '&').ToList();
            items.RemoveAt(0);
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (items.Count >= 1)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    dic.Add(items[i].Split('=')[0], items[i].Split('=')[1]);
                }
            }

            return dic;
           
        }


        public static string ToCurrency(this double numeric)
        {
            try
            {
                return "₦" + numeric.ToString("C0").Substring(1, numeric.ToString("C0").Length - 1);
            }
            catch
            {
                return "₦0";
            }
            
        }

        public static string ToRelativeTime(this DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("about {0} {1} ago",
                years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("about {0} {1} ago",
                months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format("about {0} {1} ago",
                span.Days, span.Days == 1 ? "day" : "days");
            if (span.Hours > 0)
                return String.Format("about {0} {1} ago",
                span.Hours, span.Hours == 1 ? "hour" : "hours");
            if (span.Minutes > 0)
                return String.Format("about {0} {1} ago",
                span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format("about {0} seconds ago", span.Seconds);
            if (span.Seconds <= 5)
                return "just now";
            return string.Empty;
        }



        


    }
}