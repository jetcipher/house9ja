﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;

namespace HNApp.Utils
{
    public class ListObservableCollection<T> : ObservableCollection<T>
    {
        public ListObservableCollection() : base()
        {

        }


        public ListObservableCollection(IEnumerable<T> collection) : base(collection)
        {

        }


        public ListObservableCollection(List<T> list) : base(list)
        {

        }
        public static implicit operator ListObservableCollection<T>(List<T> val)
        {
            return new ListObservableCollection<T>(val);
        }
    }
}