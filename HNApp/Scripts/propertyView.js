﻿$(document).ready(function () {
    var val = $("#price").html();
    $("#price").html(parseInt(val).formatMoney(0, "₦ "));


    $('.slider.slider-property-gallery').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows: false,
        fade: true,
        infinite: false,
        asNavFor: '.property-gallery-pager'
    });

    $('.property-gallery-pager').on('init', function (event, slick) {
        $('.slide-counter').text('1 / ' + slick.slideCount + " click on image to zoom");
    });

    $('.property-gallery-pager').slick({
        prevArrow: $('.slider-nav-property-gallery .slider-prev'),
        nextArrow: $('.slider-nav-property-gallery .slider-next'),
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider.slider-property-gallery',
        dots: false,
        focusOnSelect: true,
        infinite: false
    });

    $('.property-gallery-pager').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        currentSlide = currentSlide + 1;
        var counter = currentSlide + ' / ' + slick.slideCount;
        $('.slide-counter').text(counter + " click on image to zoom");
    });

    //INITIATE SLIDES
    $('.slide').addClass('initialized');


        $(".slider.slider-property-gallery").lightGallery({
            selector: '.slide'
        });


    $('.copy-button').tooltip();

    $('.copy-button').bind('click', function () {
        var input = $(this).parent().parent().children(":first")[0];
        
        input.select();
        try {
            var success = document.execCommand('copy');
            if (success) {
                $(this).trigger('copied', ['Copied!']);
            } else {
                $(this).trigger('copied', ['Copy with Ctrl-c']);
            }
        } catch (err) {
            $(this).trigger('copied', ['Copy with Ctrl-c']);
        }
    });

    $('.copy-button').bind('copied', function (event, message) {
        $(this).attr('title', message)
            .tooltip('fixTitle')
            .tooltip('show')
            .attr('title', "Copy to Clipboard")
            .tooltip('fixTitle');
    });
});


$(".tab_content").hide();
$(".tab_content:first").show();

/* if in tab mode */
$("ul.tabs li").click(function () {

    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");

    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

});
/* if in drawer mode */
$(".tab_drawer_heading").click(function () {

    console.log($(".tab_content").is(":visible"));
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeToggle();
   

    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");

    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
});


/* Extra class "tab_last" 
   to add border to right side
   of last tab */
$('ul.tabs li').last().addClass("tab_last");



