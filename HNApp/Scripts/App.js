﻿var app = angular.module("HouseApp", ["ngMaterial", "ngResource", "ui-rangeSlider", "ODataResources"]);
app.config(["$mdThemingProvider",function ($mdThemingProvider) {
    $mdThemingProvider.theme("default")
        .primaryPalette("blue")
        .accentPalette("grey");
}]);

app.factory("Api", ["$odataresource","$resource", function ($odataresource, $resource) {
    var factory = {};
    factory.prop = function () { return $odataresource("/api/PropertyListings:id"); };
    factory.nprop = function () { return $resource("/api/PropertyListings/:id", null, { "update": { method: "PUT" } }); };
    factory.nserv = function () { return $resource("/api/ServiceListings/:id", null, { "update": { method: "PUT" } }); };
    factory.loc = function () { return $odataresource("/api/Locations:id"); };
    factory.state = function () { return $odataresource("/api/Locations/states"); };
    factory.citlg = function (state) { return $odataresource("/api/Locations/" + state + "/citlgas"); };
    factory.areas = function (state, cl) { return $odataresource("/api/Locations/" + state + "/citlgas/" + cl + "/areas"); };
    factory.cats = function () { return $odataresource("/api/categories:id"); };
    factory.services = function () { return $odataresource("/api/Services:id"); };
    return factory;
}]);

app.directive("convertToNumber",[ function () {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val != null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function (val) {
                return val != null ? "" + val : null;
            });
        }
    };
}]);

app.filter("toWords",[ function () {

    // In the return function, we must pass in a single parameter which will be the data we will work on.
    // We have the ability to support multiple other parameters that can be passed into the filter optionally
    return function (input) {



        return int_to_words(parseInt(input));

    };
}]);

app.filter("to_local_date",[ function () {
    return function (text) {
        return prettyDate(text);
    };
}]);

app.filter("defText",[ function () {
    return function (input, defaultValue) {
        if (input === "All") {
            return defaultValue;
        }

        return input;
    };
}]);

app.filter("defUrl", [function () {
    return function (input, title) {


        return "/property/" + input + "-" + title.split(" ").join("-");
    };
}]);



app.filter("defPath", [function () {
    return function (input, title) {


        return input === undefined ? title : input;
    };
}]);



app.filter("contr", [function () {
    return function (input) {
        if (parseInt(input, 0) == 0) {
            return "Rent";
        }
        else {
            return "Sale";
        }
    };
}]);

app.controller("mainCntrl", ["$scope","Api", function ($scope, Api) {
    $scope.priceMod = {
        range: {
            min: 0,
            max: 1000000000
        },
        minPrice: 0,
        maxPrice: 1000000000
    };
    $scope.facMode = "full";
    $scope.display = "grid";
    $scope.InitSort = function () {
        if (typeof (Storage) !== "undefined") {
            $scope.display = (localStorage.getItem("display") == null) ? "grid" : localStorage.getItem("display");
        }
        //  angular.element($scope.display).addClass("sactive");
        document.getElementById($scope.display).classList.add("active");
    };

    $scope.Sorter = function (val) {
        localStorage.setItem("display", val);
        location.reload(true);
    };


    $("#min").keyup(function () {
        var val = $(this).val().replace(/[^0-9]/g, "");

        var scope = angular.element($(this)).scope();
        scope.$apply(function () {
            scope.priceMod.minPrice = parseInt(val);
        });
        if (val == 0) return;
        $(this).val(parseInt(val).formatMoney(0, "₦ "));
    });


    $("#max").keyup(function () {
        var val = $(this).val().replace(/[^0-9]/g, "");

        var scope = angular.element($(this)).scope();
        scope.$apply(function () {
            scope.priceMod.maxPrice = parseInt(val);
        });
        if (val == 0) return;
        $(this).val(parseInt(val).formatMoney(0, "₦ "));
    });


    $scope.initPropertyStart = function () {
        $scope.state = "All";
        $scope.city = "All";
        $scope.area = "All";
        $scope.cities = new Array();
        $scope.cities.push("All");
        $scope.areas = new Array();
        $scope.areas.push("All");
        $scope.categorey = "All";
        $scope.type = "All";
        $scope.facilities = 0;
        //$scope.states = Api.state().odata().query(function () {
        //    $scope.states.unshift("All");
        //});
        $scope.cats = Api.cats().odata().query(function () {
            $scope.cats.unshift({ Name: "All" });
            $scope.catCheck($scope.categorey);
        });
        $scope.states = Api.state().odata().query(function () {
            $scope.states.unshift("All");
            if ($scope.state != "All") $scope.stChange($scope.state, 1);
        });
    };


    $scope.initPropertyEdit = function (state, city, area, categorey, type, facilities, minPrice, maxPrice, order, contract) {
        $scope.facMode = "init";
        $scope.order = order;
        $scope.priceMod.minPrice = minPrice;
        $scope.priceMod.maxPrice = maxPrice;
        $scope.state = state.split("_").join(" ");
        $scope.city = city.split("_").join(" ");
        $scope.area = area.split("_").join(" ");
        $scope.cities = new Array();
        $scope.cities.push("All");
        $scope.areas = new Array();
        $scope.areas.push("All");
        $scope.categorey = categorey;
        $scope.type = type;
        $scope.facilities = facilities;
        $scope.contract = contract;

        //$scope.states = Api.state().odata().query(function () {
        //    $scope.states.unshift("All");
        //});
        $scope.cats = Api.cats().odata().query(function () {
            $scope.cats.unshift({ Name: "All" });
            $scope.catCheck(1);
        });
        $scope.states = Api.state().odata().query(function () {
            $scope.states.unshift("All");
            if ($scope.state != "All") $scope.stChange($scope.state, 1);
        });


        var options = $("#fac option");

        var values = $.map(options, function (option) {
            var val = parseInt(option.value, 10);
            if ((facilities & val) != val) return;
            return option.value;
        });
        var $selc = $("#fac").smartselect({
            multiple: true,
            style: {
                select: "dropdown-toggle btn btn-default"
            },
            text: {
                selectLabel: "Select Facilities..."
            },
            toolbar: false,
            callback: {
                onOptionSelected: function (val) {
                    $scope.facChecked(val.get(0).dataset.value);
                },
                onOptionDeselected: function (val) {
                    $scope.facChecked(val.get(0).dataset.value);
                }
            }
        }).getsmartselect();

        $selc.selectOptions(values, false);
        $scope.facMode = "full";
    };


    $scope.catCheck = function (mode) {
        if ($scope.categorey != null && $scope.categorey != "All") {
            var obj = search($scope.cats, $scope.categorey, "Name"); 
            var ntypes = obj.Types;
            if (ntypes.indexOf("All") == -1) {
                ntypes.push("All");
            }

            $scope.types = ntypes;
            if (mode == undefined) $scope.type = "All";
        } else {
            $scope.types = new Array();
            $scope.types.push("All");
            $scope.type = "All";
        }
    };


    $scope.proceedProp = function () {
        var contract = $("input[name=contract]:checked").val();
        var url = "/properties/" + $scope.categorey.split(" ").join("-") + "/" + $scope.type.split(" ").join("-") + "?state=" + $scope.state.split(" ").join("_") + "&city=" + $scope.city.split(" ").join("_") + "&area=" + $scope.area.split(" ").join("_")

        if (contract != "" && contract != "-1") {
            url += "&contract=" + contract;
        }
        if ($scope.facilities != 0) {
            url += "&facilities=" + $scope.facilities;
        }
        if ($scope.priceMod.maxPrice != 1000000000) {
            url += "&priceRange=" + $scope.priceMod.minPrice + "-" + $scope.priceMod.maxPrice;
        }




        window.location.href = url;
        //}e
    };


    $scope.proceedEditProp = function () {

        var url = "/properties/" + $scope.categorey.split(" ").join("-") + "/" + $scope.type.split(" ").join("-") + "?state=" + $scope.state.split(" ").join("_") + "&city=" + $scope.city.split(" ").join("_") + "&area=" + $scope.area.split(" ").join("_")

        if ($scope.contract != "" && $scope.contract != "-1") {
            url += "&contract=" + $scope.contract;
        }
        if ($scope.facilities != 0) {
            url += "&facilities=" + $scope.facilities;
        }
        if (($scope.priceMod.maxPrice != 0) && ($scope.priceMod.maxPrice != 1000000000)) {
            url += "&priceRange=" + $scope.priceMod.minPrice + "-" + $scope.priceMod.maxPrice;
        }

        if ($scope.order !== undefined || $scope.order !== "0") {
            url += "&order=" + $scope.order;
        }


        window.location.href = url;
        //}e
    };



    $scope.clearLoc = function () {
        $scope.state = "All";
        $scope.city = "All";
        $scope.area = "All";
        $scope.cities = new Array();
        $scope.cities.push("All");
        $scope.areas = new Array();
        $scope.areas.push("All");
    };
    $scope.stChange = function (mode) {
        if ($scope.state != "All") {
            $scope.cities = Api.citlg($scope.state).odata().query(function () {
                $scope.cities.unshift("All");
                if (mode == undefined) {
                    $scope.city = $scope.cities[0];
                    $scope.ctChange();
                }
                else $scope.ctChange(mode);


            });
        }
        else {
            $scope.cities = new Array();
            $scope.cities.push("All");
            $scope.city = "All";
            $scope.areas = new Array();
            $scope.areas.push("All");
            $scope.area = "All";
        }


    };

    $scope.facChecked = function (val) {
        if ($scope.facMode != "full") return;
        if (($scope.facilities & val) == val) {
            $scope.facilities &= ~parseInt(val);
        }
        else {
            $scope.facilities |= parseInt(val);
        }
    };
    $scope.ctChange = function (mode) {
        if ($scope.city != null && $scope.city != "All") {
            $scope.areas = Api.areas($scope.state, $scope.city).odata().query(function () {
                $scope.areas.unshift("All");
                if (mode == undefined) {
                    $scope.area = $scope.areas[0];
                }

            });
        } else {
            $scope.areas = new Array();
            $scope.areas.push("All");
            $scope.area = "All";
        }

    };




    $scope.initMain = function () {
        $scope.props = Api.prop().odata().orderBy("ViewCount", "desc").skip(0)
            .take(6).query();
    };


}]);



app.controller("serviceSearchCntrl", ["$scope","Api",function ($scope, Api) {

    $scope.initStart = function () {
        $scope.state = "All";
        $scope.city = "All";
        $scope.area = "All";
        $scope.cities = new Array();
        $scope.cities.push("All");
        $scope.areas = new Array();
        $scope.areas.push("All");
        $scope.name = "";
        $scope.type = "All";
        $scope.order = "0";

        $scope.services = Api.services().odata().query(function () {
            $scope.services.unshift({ Title: "All" });
        });
        $scope.states = Api.state().odata().query(function () {
            $scope.states.unshift("All");
            if ($scope.state != "All") $scope.stChange($scope.state, 1);
        });
    };


    $scope.initEdit = function (state, city, area, name, type, order) {
        $scope.state = state;
        $scope.city = city;
        $scope.area = area;
        $scope.cities = new Array();
        $scope.areas = new Array();
        $scope.name = name;
        $scope.type = type;
        $scope.order = order;
        if (state == "All") {
            $scope.cities.push("All");
            $scope.areas.push("All");
        }

        $scope.services = Api.services().odata().query(function () {
            $scope.services.unshift({ Title: "All" });
        });
        $scope.states = Api.state().odata().query(function () {
            $scope.states.unshift("All");
            if ($scope.state != "All") $scope.stChange($scope.state, 1);
        });
    };

    $scope.GetData = function (val) {
        $.get("/service/servicenames/" + val, function (data) {
            $("#" + val).append(data);
        });
        //$("#" + val).append(val + "val");
    };

    $scope.proceed = function () {
        var url = "/services/" + $scope.type.split(" ").join("-") + "?state=" + $scope.state.split(" ").join("_");
        if ($scope.city != "All") {
            url += "&city=" + $scope.city.split(" ").join("_");
        }
        if ($scope.area != "All") {
            url += "&area=" + $scope.area.split(" ").join("_");
        }
        if ($scope.name != "") {
            url += "&name=" + $scope.name;
        }

        if ($scope.order != "0") {
            url += "&order=" + $scope.order;
        }

        window.location.href = url;
    };

    $scope.clearLoc = function () {
        $scope.state = "All";
        $scope.city = "All";
        $scope.area = "All";
        $scope.cities = new Array();
        $scope.cities.push("All");
        $scope.areas = new Array();
        $scope.areas.push("All");
    };
    $scope.stChange = function (mode) {
        if ($scope.state != "All") {
            $scope.cities = Api.citlg($scope.state).odata().query(function () {
                $scope.cities.unshift("All");
                if (mode == undefined) {
                    $scope.city = $scope.cities[0];
                    $scope.ctChange();
                }
                else $scope.ctChange(mode);


            });
        }
        else {
            $scope.cities = new Array();
            $scope.cities.push("All");
            $scope.city = "All";
            $scope.areas = new Array();
            $scope.areas.push("All");
            $scope.area = "All";
        }


    };
    $scope.ctChange = function (mode) {
        if ($scope.city != null && $scope.city != "All") {
            $scope.areas = Api.areas($scope.state, $scope.city).odata().query(function () {
                $scope.areas.unshift("All");
                if (mode == undefined) {
                    $scope.area = $scope.areas[0];
                }

            });
        } else {
            $scope.areas = new Array();
            $scope.areas.push("All");
            $scope.area = "All";
        }

    };
}]);



app.controller("propAddCntrl", ["$scope", "Api","$mdDialog",function ($scope, Api, $mdDialog) {
    $scope.curCat = {};
    $scope.init = function (name, adr, phn, mail, id) {
        $scope.prop = {};
        $scope.prop.Location = {};
        $scope.states = Api.state().odata().query(function () {
            $scope.prop.Location.State = $scope.states[1];
            $scope.stChange($scope.prop.Location.State);
        });
        $scope.cats = Api.cats().odata().query(function () {
            $scope.categoreyId = $scope.cats[0].Id;
            $scope.catCheck($scope.categoreyId);
        });
        $scope.prop.Descriptions = new Array();
        $scope.prop.Photos = new Array();
        $scope.prop.Contact = { Name: name, PhoneNumber: phn, Email: mail, Address: adr, Source: 0 };
        $scope.prop.AuthorId = id;
    };

    $scope.catCheck = function (val) {
        console.log(val);
        $scope.prop.CategoreyId = val;
        $scope.types = search($scope.cats, val, "Id").Types;
        if ($scope.types.indexOf("All") == -1) $scope.types.unshift("All");
        $scope.alds = search($scope.cats,val,"Id").AllowedDescriptions;
        $scope.catName = search($scope.cats, val, "Id").Name;
        $scope.prop.TypeId = 0;
    };

    $scope.stChange = function (val) {
        $scope.citlgas = Api.citlg(val).odata().query(function () {
            $scope.prop.Location.CityLGA = $scope.citlgas[0];
            $scope.clChange();
        });

    };


    $scope.clChange = function () {
        if ($scope.prop.Location.CityLGA != null) {
            $scope.areas = Api.areas($scope.prop.Location.State, $scope.prop.Location.CityLGA).odata().query(function () {
                $scope.prop.Location.Area = $scope.areas[0];
            });
        }

    };
    $scope.facChecked = function (val) {

        if (($scope.prop.Facilities & val) == val) {
            $scope.prop.Facilities &= ~parseInt(val);
        }
        else {
            $scope.prop.Facilities |= parseInt(val);
        }
    };

    $scope.showAdd = function () {
        $scope.state = "Create";
        $scope.dcrp = { Name: $scope.alds[0] };
        $("#addPropDialog").modal("show");
        //$mdDialog.show({
        //    contentElement: "#addPropDialog",
        //    parent: angular.element(document.body),
        //    clickOutsideToClose: true
        //});
    };
    $scope.showEdit = function (id) {
        $scope.state = "Edit";
        $scope.curIndex = id;
        $scope.dcrp = $scope.prop.Descriptions[id];
        $("#addPropDialog").modal("show");
    };

    $scope.saveDcrp = function (val) {
        if ($scope.state == "Edit") {
            $("#addPropDialog").modal("hide");
        } else {
            if ((val.Value != undefined) && (val.Value != "")) {
                $("#addPropDialog").modal("hide");
                $scope.prop.Descriptions.push(val);
            }
        }

    };

    $scope.del = function (id) {
        $scope.prop.Descriptions.splice(id, 1);
    };

    $scope.addPhoto = function (url) {
        $scope.prop.Photos.push(url);
        console.log($scope.prop.Photos);
    };

    $scope.removePhoto = function (url) {
        var index = $scope.prop.Photos.indexOf(url);
        $scope.prop.Photos.splice(index, 1);
    };

    $scope.getLoc = function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (value) {
                var cord = {};
                cord.Latitude = value.coords.latitude;
                cord.Longitude = value.coords.longitude;
                $scope.prop.Location.Cordinate = { Longitude: cord.Longitude, Latitude: cord.Latitude };
                $("#cord").val(cord.Latitude + "," + cord.Longitude);
            });
        } else {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector("Body")))
                    .clickOutsideToClose(true)
                    .title("Failed")
                    .textContent("Geolocation is not supported by this browser.")
                    .ariaLabel("Failed")
                    .ok("ok")
            );
        }
    };

    $scope.addFile = function (url) {
        $scope.prop.FilePath = url;
    };
    $scope.removeFile = function () {
        $scope.prop.FilePath = "";
    };


    $scope.addVideo = function (url) {
        $scope.prop.VideoPath = url;
    };
    $scope.removeVideo = function () {
        $scope.prop.VideoPath = "";
    };

    $scope.saveProp = function () {
        var f1 = $("#form1")[0].checkValidity();
        var f2 = $("#form2")[0].checkValidity();
        if (f1 && f2) {
            $scope.prop.TypeId -= 1;

            Api.nprop().save($scope.prop, function (data) {
                window.location.href = "/property/" + data.Id + "-" + data.Title.split(" ").join("-");
            }, function () {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector("Body")))
                        .clickOutsideToClose(true)
                        .title("Failed")
                        .textContent("The item failed. Make sure u have a stable connection")
                        .ariaLabel("Failed")
                        .ok("ok")
                );
            });



        }
        else {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector("Body")))
                    .clickOutsideToClose(true)
                    .title("Failed")
                    .textContent("Make sure you have added all the required fields.")
                    .ariaLabel("Failed")
                    .ok("ok")
            );
        }
    };

}]);


app.controller("propEditCntrl", ["$scope", "Api", "$mdDialog", function ($scope, Api, $mdDialog) {
    $scope.curCat = {};

    $scope.facMode = "full";

    $scope.init = function (id) {
        $scope.facMode = "init";

        Api.nprop().get({ id: id }, function (data) {
            $scope.prop = data;

            $scope.cats = Api.cats().odata().query(function () {
                $scope.categoreyId = $scope.prop.CategoreyId;
                $scope.prop.TypeId += 1;
                $scope.alds = $scope.cats[0].AllowedDescriptions;
                $scope.catCheck($scope.prop.CategoreyId, 1);
            });
            if ($scope.prop.Location.Cordinate != null) $("#cord").val($scope.prop.Location.Cordinate.Latitude + "," + $scope.prop.Location.Cordinate.Longitude);
            var options = $(".treeFac option");

            var values = $.map(options, function (option) {
                var val = parseInt(option.value, 10);
                if ((data.Facilities & val) != val) return;
                return option.value;
            });
            var $selc = $(".treeFac").smartselect({
                multiple: true,
                style: {
                    select: "dropdown-toggle btn btn-default"
                },
                text: {
                    selectLabel: "Select Facilities..."
                },
                toolbar: false,
                callback: {
                    onOptionSelected: function (val) {
                        angular.element(document.getElementById("submitProperty")).scope().facChecked(val.get(0).dataset.value);
                    },
                    onOptionDeselected: function (val) {
                        angular.element(document.getElementById("submitProperty")).scope().facChecked(val.get(0).dataset.value);
                    }
                }
            }).getsmartselect();

            $selc.selectOptions(values, false);
            $scope.facMode = "full";
            getPhotos();
            getVideo();
            getFile();

            $scope.states = Api.state().odata().query(function () {
                $scope.stChange($scope.prop.Location.State, 1);
            });

        });



    };


    function getPhotos() {
        var myDropzone = Dropzone.forElement("#hnImageUpload");
        for (var i = 0; i < $scope.prop.Photos.length; i++) {
            var url = $scope.prop.Photos[i];

            var mockFile = { name: "Image", size: 1, url: url };
            myDropzone.emit("addedfile", mockFile);

            myDropzone.emit("thumbnail", mockFile, url);

            myDropzone.emit("complete", mockFile);


        }
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - $scope.prop.Photos.length;
    };


    function getVideo() {
        var myDropzone = Dropzone.forElement("#hnVideoUpload");
        var url = $scope.prop.VideoPath;
        if (url != undefined && url != "") {
            var mockFile = { name: "Video", size: 1, url: url };
            myDropzone.emit("addedfile", mockFile);

            //  myDropzone.emit("thumbnail", mockFile, url);

            myDropzone.emit("complete", mockFile);
            myDropzone.options.maxFiles = myDropzone.options.maxFiles - 1;
        }

    };

    function getFile() {
        var myDropzone = Dropzone.forElement("#hnDocUpload");
        var url = $scope.prop.FilePath;
        if (url != undefined && url != "") {
            var mockFile = { name: "Document", size: 1, url: url };
            myDropzone.emit("addedfile", mockFile);

            //   myDropzone.emit("thumbnail", mockFile, url);

            myDropzone.emit("complete", mockFile);
            myDropzone.options.maxFiles = myDropzone.options.maxFiles - 1;
        }
    };


    $scope.catCheck = function (val, iv) {
        $scope.prop.CategoreyId = val;
        $scope.types = search($scope.cats, val, "Id").Types;
        $scope.alds = search($scope.cats, val, "Id").AllowedDescriptions;
        $scope.catName = search($scope.cats, val, "Id").Name;
        if ($scope.types.indexOf("All") == -1) $scope.types.unshift("All");
        if (iv == undefined) $scope.prop.TypeId = 0;
    };

    $scope.stChange = function (val, mode) {
        $scope.citlgas = Api.citlg(val).odata().query(function () {
            if (mode == undefined) {
                $scope.prop.Location.CityLGA = $scope.citlgas[0];
                $scope.clChange();
            }
            else $scope.clChange(mode);


        });

    };


    $scope.clChange = function (mode) {
        if ($scope.prop.Location.CityLGA != null) {
            $scope.areas = Api.areas($scope.prop.Location.State, $scope.prop.Location.CityLGA).odata().query(function () {
                if (mode == undefined) {
                    $scope.prop.Location.Area = $scope.areas[0];
                }

            });
        }

    };
    $scope.facChecked = function (val) {
        if ($scope.facMode != "full") return;
        if (($scope.prop.Facilities & val) == val) {
            $scope.prop.Facilities &= ~parseInt(val);
        }
        else {
            $scope.prop.Facilities |= parseInt(val);
        }
    };

    $scope.showAdd = function () {
        $scope.state = "Create";
        $scope.dcrp = { Name: $scope.alds[0] };
        $("#addPropDialog").modal("show");
    };
    $scope.showEdit = function (id) {
        $scope.state = "Edit";
        $scope.curIndex = id;
        $scope.dcrp = $scope.prop.Descriptions[id];
        $("#addPropDialog").modal("show");
    };

    $scope.saveDcrp = function (val) {
        if ($scope.state == "Edit") {
            $("#addPropDialog").modal("hide");
        } else {
            if ((val.Value != undefined) && (val.Value != "")) {
                $mdDialog.hide();
                $("#addPropDialog").modal("hide");
                $scope.prop.Descriptions.push(val);
            }
        }

    };

    $scope.del = function (id) {
        $scope.prop.Descriptions.splice(id, 1);
    };

    $scope.addPhoto = function (url) {
        $scope.prop.Photos.push(url);
    };

    $scope.removePhoto = function (url, type) {
        var index = $scope.prop.Photos.indexOf(url);
        $scope.prop.Photos.splice(index, 1);

        if (type == 1) {
            var myDropzone = Dropzone.forElement("#hnImageUpload");
            myDropzone.options.maxFiles = myDropzone.options.maxFiles + 1;
        }
    };

    $scope.getLoc = function () {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (value) {
                var cord = {};
                cord.Latitude = value.coords.latitude;
                cord.Longitude = value.coords.longitude;
                if ($scope.prop.Location.Cordinate == undefined) $scope.prop.Location.Cordinate = { Longitude: cord.Longitude, Latitude: cord.Latitude };
                else {
                    $scope.prop.Location.Cordinate.Latitude = cord.Latitude;
                    $scope.prop.Location.Cordinate.Longitude = cord.Longitude;
                }
                $("#cord").val(cord.Latitude + "," + cord.Longitude);
            });
        } else {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector("Body")))
                    .clickOutsideToClose(true)
                    .title("Failed")
                    .textContent("Geolocation is not supported by this browser.")
                    .ariaLabel("Failed")
                    .ok("ok")
            );
        }
    };

    $scope.addFile = function (url) {
        $scope.prop.FilePath = url;
    };
    $scope.removeFile = function (type) {
        $scope.prop.FilePath = "";
        if (type == 1) {
            var myDropzone = Dropzone.forElement("#hnDocUpload");
            myDropzone.options.maxFiles = myDropzone.options.maxFiles + 1;
        }
    };


    $scope.addVideo = function (url) {
        $scope.prop.VideoPath = url;
    };
    $scope.removeVideo = function (type) {
        $scope.prop.VideoPath = "";
        if (type == 1) {
            var myDropzone = Dropzone.forElement("#hnVideoUpload");
            myDropzone.options.maxFiles = myDropzone.options.maxFiles + 1;
        }
    };

    $scope.saveProp = function () {
        var f1 = $("#form1")[0].checkValidity();
        var f2 = $("#form2")[0].checkValidity();

        if (f1 && f2) {
            if ($scope.prop.TypeId >= 0) $scope.prop.TypeId -= 1;
            Api.nprop().update({ id: $scope.prop.Id }, $scope.prop, function (value) {
                window.location.href = "/property/" + $scope.prop.Id + "-" + $scope.prop.Title.split(" ").join("-");
            }, function () {

                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector("Body")))
                        .clickOutsideToClose(true)
                        .title("Failed")
                        .textContent("The item failed.")
                        .ariaLabel("Failed")
                        .ok("ok")
                );
            });


        }
        else {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector("Body")))
                    .clickOutsideToClose(true)
                    .title("Failed")
                    .textContent("Make sure you have added all the required fields.")
                    .ariaLabel("Failed")
                    .ok("ok")
            );
        }
    };

}]);


app.controller("contactCntrl", ["$scope", "$mdDialog","Api",function ($scope, $mdDialog, Api) {
    $scope.showContact = function () {
        // $mdDialog.show({
        //     contentElement: "#contactDialog",
        //    parent: angular.element(document.body),
        //    clickOutsideToClose: true
        //});
        $("#contactDialog").modal();
    };


    $scope.close = function () {
        $("#contactDialog").modal("hide");
    };

    $scope.init = function (id) {
        //Api.nprop().get({ id: $scope.prop.Id })
        //Api.nprop().get({ id: id }, function (data) {
        //    data.ViewCount += 1;
        //    Api.nprop().update({ id: data.Id }, data);
        //});
        $.get("/listing/increment?id="+id, function (data) {});


    };

}]);


app.controller("serviceAdd", ["$scope", "Api", "$mdDialog", function ($scope, Api, $mdDialog) {

    $scope.addPhoto = function (url) {
        $scope.serv.ImagePath = url;
    };

    $scope.removePhoto = function (url) {
        $scope.serv.ImagePath = "";
    };


    $scope.init = function (id) {
        $scope.verify = 0;
        $scope.serv = {};
        $scope.serv.Location = {};
        $scope.states = Api.state().odata().query(function () {
            $scope.serv.Location.State = $scope.states[1];
            $scope.stChange($scope.serv.Location.State);
        });
        $scope.svs = Api.services().odata().query(function () {
            var $selc = $(".treeFac").smartselect({
                multiple: true,
                style: {
                    select: "dropdown-toggle btn btn-default"
                },
                text: {
                    selectLabel: "Select service(s)..."
                },
                toolbar: false,
                callback: {
                    onOptionSelected: function (val) {
                        var curid = val.get(0).dataset.value;
                        toggleSelc(curid);
                    },
                    onOptionDeselected: function (val) {
                        var curid = val.get(0).dataset.value;
                        toggleSelc(curid);
                    }
                },
                atLeast: 1,
                handler: {
                    atLeast: function () {
                        alert("at least 1 option required");
                    }
                }
            }).getsmartselect();
            for (var i = 0; i < $scope.svs.length; i++) {
                $selc.addOption({
                    label: $scope.svs[i].Title,
                    value: $scope.svs[i].Id
                });
            }
        });
        $scope.serv.ServiceIds = new Array();
        $scope.serv.Contact = {};
        $scope.serv.AuthorId = id;
    };


    $scope.saveServ = function () {
        var f1 = $("#form1")[0].checkValidity();
        var f2 = $("#form2")[0].checkValidity();
        if (f1 && f2 && ($scope.serv.ServiceIds.length >= 1)) {
            Api.nserv().save($scope.serv, function (data) {
                window.location.href = "/service/" + data.Id + "-" + data.Title.split(" ").join("-");

            }, function () {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector("Body")))
                        .clickOutsideToClose(true)
                        .title("Failed")
                        .textContent("The item failed.")
                        .ariaLabel("Failed")
                        .ok("ok")
                );
            });


        }
        else {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector("Body")))
                    .clickOutsideToClose(true)
                    .title("Failed")
                    .textContent("Make sure you have added all the required fields.")
                    .ariaLabel("Failed")
                    .ok("ok")
            );
        }
    };

    function toggleSelc(id) {
        //$scope.serv.ServiceIds.push(1);
        var index = $scope.serv.ServiceIds.indexOf(id);
        if (index == -1) {
            $scope.serv.ServiceIds.push(id);
        } else {
            $scope.serv.ServiceIds.splice(index, 1);
        }
    }


    $scope.stChange = function (val) {
        $scope.citlgas = Api.citlg(val).odata().query(function () {
            $scope.serv.Location.CityLGA = $scope.citlgas[0];
            $scope.clChange();
        });

    };


    $scope.clChange = function () {
        if ($scope.serv.Location.CityLGA != null) {
            $scope.areas = Api.areas($scope.serv.Location.State, $scope.serv.Location.CityLGA).odata().query(function () {
                $scope.serv.Location.Area = $scope.areas[0];
            });
        }

    };


}]);


app.controller("serviceEdit", ["$scope", "Api", "$mdDialog",function ($scope, Api, $mdDialog) {
    $scope.addPhoto = function (url) {
        $scope.serv.ImagePath = url;
    };

    $scope.removePhoto = function (url, type) {
        $scope.serv.ImagePath = "";
        if (type == 1) {
            var myDropzone = Dropzone.forElement("#hnImageUpload");
            myDropzone.options.maxFiles = myDropzone.options.maxFiles + 1;
        }
    };


    $scope.init = function (id) {
        $scope.facMode = "init";
        $scope.svs = Api.services().odata().query(function () {
            var $selc = $(".treeFac").smartselect({
                multiple: true,
                style: {
                    select: "dropdown-toggle btn btn-default"
                },
                text: {
                    selectLabel: "Select service(s)..."
                },
                toolbar: false,
                callback: {
                    onOptionSelected: function (val) {
                        var curid = val.get(0).dataset.value;
                        toggleSelc(curid);
                    },
                    onOptionDeselected: function (val) {
                        var curid = val.get(0).dataset.value;
                        toggleSelc(curid);
                    }
                },
                atLeast: 1,
                handler: {
                    atLeast: function () {
                        alert("at least 1 option required");
                    }
                }
            }).getsmartselect();
            for (var i = 0; i < $scope.svs.length; i++) {
                $selc.addOption({
                    label: $scope.svs[i].Title,
                    value: $scope.svs[i].Id
                });
            }
            Api.nserv().get({ id: id }, function (data) {
                $scope.serv = data;

                $selc.selectOptions(data.ServiceIds, false);


                //$selc.selectOptions(values, false);
                $scope.facMode = "full";
                getPhoto();
                $scope.states = Api.state().odata().query(function () {
                    $scope.stChange($scope.serv.Location.State, 1);
                });

            });

        });




    };



    function getPhoto() {
        if (($scope.serv.ImagePath != undefined) && ($scope.serv.ImagePath != null) && ($scope.serv.ImagePath != "")) {
            var myDropzone = Dropzone.forElement("#hnImageUpload");
            var url = $scope.serv.ImagePath;
            if (url != undefined && url != "") {
                var mockFile = { name: "Image", size: 1, url: url };
                myDropzone.emit("addedfile", mockFile);

                myDropzone.emit("thumbnail", mockFile, url);

                myDropzone.emit("complete", mockFile);
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - 1;
            }
        }
    };


    $scope.saveServ = function () {
        var f1 = $("#form1")[0].checkValidity();
        var f2 = $("#form2")[0].checkValidity();
        if (f1 && f2 && ($scope.serv.ServiceIds.length >= 1)) {
            Api.nserv().update({ id: $scope.serv.Id }, $scope.serv, function (value) {
                console.log($scope.verify);
                if($scope.verify === 0)
                {
                    window.location.href = "/transaction/BeginVerify/" + $scope.serv.Id;
                }
            else
                {
                    //console.log("failed");
                    window.location.href = "/service/" + $scope.serv.Id + "-" + $scope.serv.Title.split(" ").join("-");
                }
                
            }, function () {

                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector("Body")))
                        .clickOutsideToClose(true)
                        .title("Failed")
                        .textContent("The item failed.")
                        .ariaLabel("Failed")
                        .ok("ok")
                );
            });

        }
        else {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector("Body")))
                    .clickOutsideToClose(true)
                    .title("Failed")
                    .textContent("Make sure you have added all the required fields.")
                    .ariaLabel("Failed")
                    .ok("ok")
            );
        }
    };

    function toggleSelc(id) {
        if ($scope.facMode != "full") return;
        var index = $scope.serv.ServiceIds.indexOf(id);
        if (index == -1) {
            $scope.serv.ServiceIds.push(id);
        } else {
            $scope.serv.ServiceIds.splice(index, 1);
        }
    }


    $scope.stChange = function (val, mode) {
        $scope.citlgas = Api.citlg(val).odata().query(function () {
            if (mode == undefined) {
                $scope.serv.Location.CityLGA = $scope.citlgas[0];
                $scope.clChange();
            }
            else $scope.clChange(mode);


        });
    };


    $scope.clChange = function (mode) {
        if ($scope.serv.Location.CityLGA != null) {
            $scope.areas = Api.areas($scope.serv.Location.State, $scope.serv.Location.CityLGA).odata().query(function () {
                if (mode == undefined) {
                    $scope.serv.Location.Area = $scope.areas[0];
                }

            });
        }

    };
}]);


function search(array, key, prop) {
    // Optional, but fallback to key['name'] if not selected
    prop = (typeof prop === 'undefined') ? 'name' : prop;

    for (var i = 0; i < array.length; i++) {
        if (array[i][prop] === key) {
            return array[i];
        }
    }
};


Number.prototype.formatMoney = function (places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};




function int_to_words(int) {
    if (int === 0) return "zero";

    var ONES = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
    var TENS = ["", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    var SCALE = ["", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion"];

    // Return string of first three digits, padded with zeros if needed
    function get_first(str) {
        return ("000" + str).substr(-3);
    }

    // Return string of digits with first three digits chopped off
    function get_rest(str) {
        return str.substr(0, str.length - 3);
    }

    // Return string of triplet convereted to words
    function triplet_to_words(_3rd, _2nd, _1st) {
        return (_3rd == "0" ? "" : ONES[_3rd] + " hundred ") + (_1st == "0" ? TENS[_2nd] : TENS[_2nd] && TENS[_2nd] + "-" || "") + (ONES[_2nd + _1st] || ONES[_1st]);
    }

    // Add to words, triplet words with scale word
    function add_to_words(words, triplet_words, scale_word) {
        return triplet_words ? triplet_words + (scale_word && " " + scale_word || "") + " " + words : words;
    }

    function iter(words, i, first, rest) {
        if (first == "000" && rest.length === 0) return words;
        return iter(add_to_words(words, triplet_to_words(first[0], first[1], first[2]), SCALE[i]), ++i, get_first(rest), get_rest(rest));
    }

    return iter("", 0, get_first(String(int)), get_rest(String(int)));
}



function prettyDate(time) {
    var date = new Date((time || "").replace(/-/g, "/").replace(/[TZ]/g, " ")),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);

    if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
        return;

    return day_diff == 0 && (
        diff < 60 && "just now" ||
        diff < 120 && "1 minute ago" ||
        diff < 3600 && Math.floor(diff / 60) + " minutes ago" ||
        diff < 7200 && "1 hour ago" ||
        diff < 86400 && Math.floor(diff / 3600) + " hours ago") ||
        day_diff == 1 && "Yesterday" ||
        day_diff < 7 && day_diff + " days ago" ||
        day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago";
}

// If jQuery is included in the page, adds a jQuery plugin to handle it as well
if (typeof jQuery != "undefined")
    jQuery.fn.prettyDate = function () {
        return this.each(function () {
            var date = prettyDate(this.title);
            if (date)
                jQuery(this).text(date);
        });
    };