﻿var app = angular.module('AdminApp', ['ngResource', 'ngMaterial']);

app.config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('grey');
});
app.factory("Api", function ($resource) {
    var factory = {};

    factory.cats = function () { return $resource("/api/Categories/:id", null, { 'update': { method: 'PUT' } }); };
    factory.types = function (cid) { return $resource("/api/Categories/" + cid + "/Types/:id", null, { 'update': { method: 'PUT' } }); };
    factory.desc = function (did) { return $resource("/api/Categories/" + did + "/Descriptions/:id", null, { 'update': { method: 'PUT' } }); };
    factory.services = function () { return $resource("/api/services/:id", null, { 'update': { method: 'PUT' } }); };
    return factory;
});

app.controller("categoreyCntrl", function ($scope,Api,$mdDialog) {
    
    $scope.init = function () {
        Api.cats().query(function (value) {
            $scope.categories = value;
        });
    };

    
    $scope.addCat = function () {
        var confirm = $mdDialog.prompt()
      .title('Add Categorey...')
      .textContent('Put the name of your categorey')
      .placeholder('Name')
      .ariaLabel('Name')
      .initialValue('')
      .ok('Add')
      .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {
            var cobj = { Id: "", Name: result, AllowedDescriptions: new Array(), Types: new Array() };
            //$scope.categories.push();
            Api.cats().save(cobj, function (val) {
                $scope.categories.push(val);
            }, function () {

                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('Body')))
                    .clickOutsideToClose(true)
                    .title('Failed')
                    .textContent('Failed to add categorey,try again!')
                    .ariaLabel('Failed')
                    .ok('ok')
                );

            });
        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };


    $scope.addType = function (index) {
        var cat = $scope.categories[index];
        var confirm = $mdDialog.prompt()
     .title('Add Type...')
     .textContent('Put the name of the type')
     .placeholder('Name')
     .ariaLabel('Name')
     .initialValue('')
     .ok('Add')
     .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {


            $.post("/api/Categories/" + cat.Id+ "/Types?type="+result)
                  .done(function (data) {

                      $scope.init();

                  }).fail(function () {
                      $mdDialog.show(
                                      $mdDialog.alert()
                                        .parent(angular.element(document.querySelector('Body')))
                                        .clickOutsideToClose(true)
                                        .title('Failed')
                                        .textContent('Failed to add type,try again!')
                                        .ariaLabel('Failed')
                                        .ok('ok')
                                    );
                  });


          
        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };

    $scope.addDesc = function (index) {
        var cat = $scope.categories[index];
        var confirm = $mdDialog.prompt()
     .title('Add Description...')
     .textContent('The allowed descriptions for listings in this categorey, e.g Size of house')
     .placeholder('Name')
     .ariaLabel('Name')
     .initialValue('')
     .ok('Add')
     .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {


            $.post("/api/Categories/" + cat.Id + "/Descriptions?desc=" + result)
                  .done(function (data) {

                      $scope.init();

                  }).fail(function () {
                      $mdDialog.show(
                                      $mdDialog.alert()
                                        .parent(angular.element(document.querySelector('Body')))
                                        .clickOutsideToClose(true)
                                        .title('Failed')
                                        .textContent('Failed to add description,try again!')
                                        .ariaLabel('Failed')
                                        .ok('ok')
                                    );
                  });



        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };

    $scope.duplicate = function (index) {
        var cat = $scope.categories[index];
        cat.Id = "";
        Api.cats().save(cat, function (val) {
            $scope.categories.push(val);
        }, function () {

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('Body')))
                .clickOutsideToClose(true)
                .title('Failed')
                .textContent('Failed to add categorey,try again!')
                .ariaLabel('Failed')
                .ok('ok')
            );

        });
    };


    $scope.editCat = function (index) {
        var cat = $scope.categories[index];
        var confirm = $mdDialog.prompt()
      .title('Edit Categorey...')
      .textContent('Put the name of your categorey')
      .placeholder('Name')
      .ariaLabel('Name')
      .initialValue(cat.Name)
      .ok('Save')
      .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {
            cat.Name = result;
            Api.cats().update({ id:cat.Id }, cat, function (val) {
                $scope.categories[index].Name = result;
            }, function () {

                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('Body')))
                    .clickOutsideToClose(true)
                    .title('Failed')
                    .textContent('Failed to update categorey,try again!')
                    .ariaLabel('Failed')
                    .ok('ok')
                );

            });
        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };

    $scope.deleteCat = function (index) {
        var cid = $scope.categories[index].Id;


        var confirm = $mdDialog.confirm()
          .title('Are you sure you want to delete?')
          .textContent('Deleting this categorey would lead to deleting all properties under this categorey')
          .ariaLabel('Delete')
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function () {
            Api.cats().delete({ id: cid }, function (val) {
                $scope.categories.splice(index, 1);
            }, function () {

                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('Body')))
                    .clickOutsideToClose(true)
                    .title('Failed')
                    .textContent('Failed to delete categorey,try again!')
                    .ariaLabel('Failed')
                    .ok('ok')
                );

            });
    }, function () {
        //nothing
    });



        
    };


    $scope.editType = function (pindex,index) {
        var cat = $scope.categories[pindex];
        var confirm = $mdDialog.prompt()
      .title('Edit Type...')
      .textContent('Put the name of your Type')
      .placeholder('Name')
      .ariaLabel('Name')
      .initialValue(cat.Types[index])
      .ok('Save')
      .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {
            $.ajax({
                url: '/api/Categories/'+cat.Id+'/types/'+index+'?type='+result,
                type: 'PUT',
                success: function (response) {
                    $scope.categories[pindex].Types[index] = result;
                    $scope.init();
                }
            });
            
        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };


    $scope.deleteType = function (pindex, index) {
        var cid = $scope.categories[pindex].Id;


        var confirm = $mdDialog.confirm()
          .title('Are you sure you want to delete?')
          .textContent('Deleting this type would lead to deleting all properties under this categorey\'s type')
          .ariaLabel('Delete')
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function () {

            $.ajax({
                       url: '/api/Categories/'+cid+'/types/'+index,
                       type: 'DELETE',
                       success: function(response) {
                           $scope.init();
                       }
                    });


            
        }, function () {
            //nothing
        });
    };



    $scope.editDsc = function (pindex,index) {
        var cat = $scope.categories[pindex];
        var confirm = $mdDialog.prompt()
      .title('Edit Description...')
      .textContent('Put the name of your Description')
      .placeholder('Name')
      .ariaLabel('Name')
      .initialValue(cat.AllowedDescriptions[index])
      .ok('Save')
      .cancel('Close');


        $mdDialog.show(confirm).then(function (result) {
            $.ajax({
                url: '/api/Categories/' + cat.Id + '/Descriptions/' + index + '?type=' + result,
                type: 'PUT',
                success: function (response) {
                    $scope.categories[pindex].AllowedDescriptions[index] = result;
                    $scope.init();
                }
            });

        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });


        
    };

    $scope.deleteDsc = function (pindex, index) {
        var cat = $scope.categories[pindex];
        cat.AllowedDescriptions.splice(index,1);
        Api.cats().update({ id: cat.Id }, cat, function (val) {
            //$scope.categories[pindex] = cat;
        }, function () {

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('Body')))
                .clickOutsideToClose(true)
                .title('Failed')
                .textContent('Failed to update categorey,try again!')
                .ariaLabel('Failed')
                .ok('ok')
            );

        });
    };

});


app.controller("usersCntrl", function ($scope, $mdDialog) {
   
    
    $scope.init = function () {
        $scope.roles = ['user', 'admin', 'moderator'];
    };

    $scope.promote = function (id, name, role) {
        $scope.Id = id;
        $scope.Name = name;
        if (role === '') {
            $scope.role = 'user';
        }
        else {
            $scope.role = role;
        }

        $mdDialog.show({
            contentElement: '#challengeDialog',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
        //console.log(window.location.search);
    };

    $scope.addRole = function () {
        
        $mdDialog.hide();
        var url = window.location.pathname + window.location.search;
        window.location.href = "/admin/promote?role=" + $scope.role + "&query=" + url +"&Id="+ $scope.Id;
    };




    $scope.ban = function (id) {
        
        $scope.Id = id;

        $mdDialog.show({
            contentElement: '#banDialog',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });

    };

    $scope.continueBan = function () {

        $mdDialog.hide();
        var url = window.location.pathname + window.location.search;
        window.location.href = "/admin/BanUnban?reason=" + $scope.reason + "&query=" + url + "&Id=" + $scope.Id + "&ban=true";
    };


    $scope.Unban = function (id) {
        $scope.Id = id;
        var confirm = $mdDialog.confirm()
          .title('Are you sure you want to unban this user?')
          .textContent('Unbanning this user, would restore login privellages')
          .ariaLabel('Unban')
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function () {

            var url = window.location.pathname + window.location.search;
            window.location.href = "/admin/BanUnban?query=" + url + "&Id=" + $scope.Id + "&ban=false";

        }, function () {
            //nothing
        });
    };

});


app.controller("stypesCntrl", function ($scope, $mdDialog,Api) {
    $scope.init = function () {
        Api.services().query(function (value) {
            $scope.serviceTypes = value;
        });
    };


    $scope.add = function () {
        var confirm = $mdDialog.prompt()
     .title('Add Service type...')
     .textContent('Put the name of the service type')
     .placeholder('Name')
     .ariaLabel('Name')
     .initialValue('')
     .ok('Add')
     .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {
            var obj = { Id: "", Title: result };
            //$scope.categories.push();
            Api.services().save(obj, function (val) {
                $scope.serviceTypes.push(val);
            }, function () {

                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('Body')))
                    .clickOutsideToClose(true)
                    .title('Failed')
                    .textContent('Failed to add service type,try again!')
                    .ariaLabel('Failed')
                    .ok('ok')
                );

            });
        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };


    $scope.edit = function (index) {
        var stp = $scope.serviceTypes[index];
        var confirm = $mdDialog.prompt()
     .title('Edit Service type...')
     .textContent('Put the name of the service type')
     .placeholder('Name')
     .ariaLabel('Name')
     .initialValue(stp.Title)
     .ok('Save')
     .cancel('Close');

        $mdDialog.show(confirm).then(function (result) {
            stp.Title = result;
            Api.services().update({id:stp.Id},stp, function (val) {
               
            }, function () {

                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('Body')))
                    .clickOutsideToClose(true)
                    .title('Failed')
                    .textContent('Failed to edit service type,try again!')
                    .ariaLabel('Failed')
                    .ok('ok')
                );

            });
        }, function () {
            //$scope.status = 'You didn\'t name your dog.';
        });
    };


    $scope.delete = function (index) {


        var confirm = $mdDialog.confirm()
          .title('Are you sure you want to delete?')
          .textContent('Deleting this type would lead to deleting all listings under this type')
          .ariaLabel('Delete')
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var stp = $scope.serviceTypes[index];
            Api.services().delete({ id: stp.Id }, function (val) {
                $scope.serviceTypes.splice(index, 1);
            }, function () {

                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('Body')))
                    .clickOutsideToClose(true)
                    .title('Failed')
                    .textContent('Failed to delete,try again!')
                    .ariaLabel('Failed')
                    .ok('ok')
                );

            });
            
        }, function () {
            //nothing
        });
        
    };


});