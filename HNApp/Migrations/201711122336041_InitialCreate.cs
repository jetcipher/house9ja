namespace HNApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BaseLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EntityStrings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        BaseLocation_Id = c.Int(),
                        CityLgaArea_Id = c.Int(),
                        Categorey_Id = c.Int(),
                        PropertyListing_Id = c.Int(),
                        ServiceListing_Id = c.Int(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseLocations", t => t.BaseLocation_Id)
                .ForeignKey("dbo.CityLgaAreas", t => t.CityLgaArea_Id)
                .ForeignKey("dbo.Categoreys", t => t.Categorey_Id)
                .ForeignKey("dbo.PropertyListings", t => t.PropertyListing_Id)
                .ForeignKey("dbo.ServiceListings", t => t.ServiceListing_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.BaseLocation_Id)
                .Index(t => t.CityLgaArea_Id)
                .Index(t => t.Categorey_Id)
                .Index(t => t.PropertyListing_Id)
                .Index(t => t.ServiceListing_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.CityLgaAreas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        BaseLocation_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseLocations", t => t.BaseLocation_Id)
                .Index(t => t.BaseLocation_Id);
            
            CreateTable(
                "dbo.Categoreys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TypeStrings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        Categorey_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categoreys", t => t.Categorey_Id)
                .Index(t => t.Categorey_Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        Email = c.String(),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        Source = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cordinates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Descriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                        PropertyListing_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PropertyListings", t => t.PropertyListing_Id)
                .Index(t => t.PropertyListing_Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Area = c.String(),
                        CityLGA = c.String(),
                        State = c.String(),
                        Cordinate_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cordinates", t => t.Cordinate_Id)
                .Index(t => t.Cordinate_Id);
            
            CreateTable(
                "dbo.PropertyListings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AdditionalText = c.String(),
                        Address = c.String(),
                        AuthorId = c.String(),
                        CategoreyId = c.Int(nullable: false),
                        Contract = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Facilities = c.Int(nullable: false),
                        FilePath = c.String(),
                        Price = c.Double(nullable: false),
                        Title = c.String(),
                        TypeId = c.Int(nullable: false),
                        VideoPath = c.String(),
                        ViewCount = c.Int(nullable: false),
                        VirtualInspectPath = c.String(),
                        Contact_Id = c.Int(),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id)
                .Index(t => t.Contact_Id)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuthorId = c.String(),
                        Comment = c.String(),
                        Date = c.DateTime(nullable: false),
                        Name = c.String(),
                        Rating = c.Int(nullable: false),
                        ServiceListing_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceListings", t => t.ServiceListing_Id)
                .Index(t => t.ServiceListing_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ServiceListings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        AuthorId = c.String(),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                        ImagePath = c.String(),
                        Title = c.String(),
                        Url = c.String(),
                        Contact_Id = c.Int(),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id)
                .Index(t => t.Contact_Id)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Address = c.String(),
                        BanReason = c.String(),
                        Facebook = c.String(),
                        Fullname = c.String(),
                        IsBanned = c.Boolean(nullable: false),
                        LinkedIn = c.String(),
                        ProfilePicPath = c.String(),
                        Twitter = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EntityStrings", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EntityStrings", "ServiceListing_Id", "dbo.ServiceListings");
            DropForeignKey("dbo.Reviews", "ServiceListing_Id", "dbo.ServiceListings");
            DropForeignKey("dbo.ServiceListings", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.ServiceListings", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.EntityStrings", "PropertyListing_Id", "dbo.PropertyListings");
            DropForeignKey("dbo.PropertyListings", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.Descriptions", "PropertyListing_Id", "dbo.PropertyListings");
            DropForeignKey("dbo.PropertyListings", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Locations", "Cordinate_Id", "dbo.Cordinates");
            DropForeignKey("dbo.TypeStrings", "Categorey_Id", "dbo.Categoreys");
            DropForeignKey("dbo.EntityStrings", "Categorey_Id", "dbo.Categoreys");
            DropForeignKey("dbo.CityLgaAreas", "BaseLocation_Id", "dbo.BaseLocations");
            DropForeignKey("dbo.EntityStrings", "CityLgaArea_Id", "dbo.CityLgaAreas");
            DropForeignKey("dbo.EntityStrings", "BaseLocation_Id", "dbo.BaseLocations");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.ServiceListings", new[] { "Location_Id" });
            DropIndex("dbo.ServiceListings", new[] { "Contact_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Reviews", new[] { "ServiceListing_Id" });
            DropIndex("dbo.PropertyListings", new[] { "Location_Id" });
            DropIndex("dbo.PropertyListings", new[] { "Contact_Id" });
            DropIndex("dbo.Locations", new[] { "Cordinate_Id" });
            DropIndex("dbo.Descriptions", new[] { "PropertyListing_Id" });
            DropIndex("dbo.TypeStrings", new[] { "Categorey_Id" });
            DropIndex("dbo.CityLgaAreas", new[] { "BaseLocation_Id" });
            DropIndex("dbo.EntityStrings", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.EntityStrings", new[] { "ServiceListing_Id" });
            DropIndex("dbo.EntityStrings", new[] { "PropertyListing_Id" });
            DropIndex("dbo.EntityStrings", new[] { "Categorey_Id" });
            DropIndex("dbo.EntityStrings", new[] { "CityLgaArea_Id" });
            DropIndex("dbo.EntityStrings", new[] { "BaseLocation_Id" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.SiteSettings");
            DropTable("dbo.Services");
            DropTable("dbo.ServiceListings");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Reviews");
            DropTable("dbo.PropertyListings");
            DropTable("dbo.Locations");
            DropTable("dbo.Descriptions");
            DropTable("dbo.Cordinates");
            DropTable("dbo.Contacts");
            DropTable("dbo.TypeStrings");
            DropTable("dbo.Categoreys");
            DropTable("dbo.CityLgaAreas");
            DropTable("dbo.EntityStrings");
            DropTable("dbo.BaseLocations");
        }
    }
}
