﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using HNApp.Models;

namespace HNApp
{
    [Newtonsoft.Json.JsonConverter(typeof(EntityStringConverter))]
    public class EntityString: Models.IKey
    {
        public int Id { get; set; }
        public string Value { get; set; }
        

        public static implicit operator EntityString(string value)
        {
            return new EntityString { Value = value };

        }

        public static bool operator ==(EntityString instance,string value)
        {
            return instance.Value == value;
        }
        public static bool operator !=(EntityString instance, string value)
        {
            return instance.Value != value;
        }

        public static string operator +(EntityString instance, string value)
        {
            return instance.Value + value;
        }

        public override string ToString()
        {
            return this.Value;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(string))
            {
                return this.Value == (string)obj;
            }
            return base.Equals(obj);
        }

        public static implicit operator string(EntityString value)
        {
            return value.Value;
        }

    }





    public class EntityStringConverter : Newtonsoft.Json.JsonConverter
    {
        public override bool CanWrite => true;
        public override bool CanRead => false;
        public override bool CanConvert(Type objectType)
        {
           
            return (objectType == typeof(EntityString) || objectType == typeof(Models.TypeString) || objectType == typeof(string));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is EntityString)
            {
               
                EntityString val = value as EntityString;

                writer.WriteValue(val.Value);
            }
            else
            {
                TypeString val = value as TypeString;
                writer.WriteValue(val.Value);
            }
            
        }
    }



   


}