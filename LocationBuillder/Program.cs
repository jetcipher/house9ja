﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using HtmlAgilityPack;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Data.Entity;
using System.Linq.Expressions;

namespace LocationBuillder
{
    class Program
    {
        
        static  void Main(string[] args)
        {
            //MongoClient client = new MongoClient("mongodb://jetcipher:%40su13201003@cluster0-shard-00-00-ak38s.mongodb.net:27017,cluster0-shard-00-01-ak38s.mongodb.net:27017,cluster0-shard-00-02-ak38s.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");
            //var lsd = client.GetDatabase("HouseNaijaDB").GetCollection<BaseLocation>("Locations");
            //lsd.InsertMany(Newtonsoft.Json.JsonConvert.DeserializeObject<List<BaseLocation>>(System.IO.File.ReadAllText(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "mydoc.txt"))));
            ////var lc = new Locationer();
            var dt = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BaseLocation>>(System.IO.File.ReadAllText(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "mydoc.txt")));
            // Console.WriteLine("Starting Process...");
            // Task.Run(async ()=> await lc.GatherStates());

            //  MyDbContext mdc = new MyDbContext();
            //mdc.People.Add(new Person { Age = 10, Name = "Jethro", State = "Lagos" });
            //mdc.People.Add(new Person { Age = 20, Name = "Mike", State = "Abia" });
            //mdc.People.Add(new Person { Age = 30, Name = "Paul", State = "Enugu" });
            //mdc.People.Add(new Person { Age = 40, Name = "Dale", State = "Abuja" });
            //mdc.SaveChanges();


            
                using (MyDbContext mc = new MyDbContext())
                {
                foreach (var item in dt)
                {
                    mc.BaseLocations.Add(item);
                    mc.SaveChanges();
                }
                    
                }
            
           
            
            //var p = mdc.People.Where(a=>a.Age > 10);
            //var dt = p.Where(a => a.Name.ToLower() == "jethro").ToList();

            Console.ReadLine();
        }

      
    }

   
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base(@"Data Source=SQL7001.site4now.net;Initial Catalog=DB_A2D1CF_jetcipher;User Id=DB_A2D1CF_jetcipher_admin;Password=su13201003;MultipleActiveResultSets=true;")
        {

        }
        public DbSet<CityLgaArea> CityLgaAreas { get; set; }
        public DbSet<BaseLocation> BaseLocations { get; set; }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string State { get; set; }
    }

    public class Locationer
    {
        private string Apiurl = "http://locationsng-api.herokuapp.com/api/v1/states/";
        private List<BaseLocation> blList;
        private List<string> urls;
        HttpClient client;
        public Locationer()
        {
            blList = new List<BaseLocation>();
            client = new HttpClient();
            urls = new List<string>();
            Console.WriteLine("Configuring.....");
        }




        //public async Task GatherStates()
        //{
        //    try
        //    {
        //        Console.WriteLine("Gathering states");
        //        HtmlWeb hw = new HtmlWeb();
        //        var doc = hw.Load("https://nigeriazipcodes.com/");
        //        var zone = doc.DocumentNode.SelectNodes("//ul[@class='sub-menu']");
                
        //        foreach (var item in zone)
        //        {
        //            var realNodes = item.ChildNodes.Where(a => a.Name == "li");
        //            foreach (var sub in realNodes)
        //            {
        //                var state = sub.InnerText.Substring(0, sub.InnerText.ToLower().IndexOf("zip") - 1).ToLower().Replace("state", "");
        //                blList.Add(new LocationBuillder.BaseLocation { AreaPredictions = new List<string>(), CityLga = new List<LocationBuillder.CityLgaArea>(), State = state });
        //                urls.Add(sub.FirstChild.Attributes["href"].Value);
        //            }
        //        }
        //        Console.WriteLine("States gathered!");

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Failed to gather states The following error occured: "+ ex.Message);
        //    }
           
        //    BeginAreaMap();
        //}

        //private void BeginAreaMap()
        //{
           
        //        Console.WriteLine("Starting Area Map....");
        //        for (int i = 0; i < urls.Count; i++)
        //        {
        //        try
        //        {
        //            HtmlWeb hw = new HtmlWeb();
        //            var doc = hw.Load(urls[i]);
        //            var h3d = doc.DocumentNode.SelectNodes("//div[@class='post-content']").FirstOrDefault().Descendants("li");
        //            Console.WriteLine($"found {i + 1} City/Lga for {blList[i].State}");
        //            foreach (var item in h3d)
        //            {
        //                try
        //                {
        //                    var cityLgaName = NameParser(item.InnerText);

        //                    if (item.FirstChild.Name != "span") blList[i].CityLga.Add(new CityLgaArea { Name = cityLgaName, Area = GetAreas(item.FirstChild.Attributes["href"].Value) });
        //                    else blList[i].CityLga.Add(new CityLgaArea { Name = cityLgaName, Area = GetAreas(item.FirstChild.FirstChild.Attributes["href"].Value) });
        //                }
        //                catch (Exception e)
        //                {

        //                    continue;
        //                }


        //            }
        //        }
        //        catch (Exception e)
        //        {
                    
        //        }
                        
                        
        //        }

        //    System.IO.File.WriteAllText(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "mydoc.txt"), Newtonsoft.Json.JsonConvert.SerializeObject(blList));
            
        //}


        private List<string> GetAreas(string url)
        {
            Console.WriteLine("Getting Areas");
            List<string> cla = new List<string>();
            HtmlWeb hw = new HtmlWeb();
            try
            {
                var doc = hw.Load(url);
                var add = (doc.DocumentNode.SelectNodes("//div[@class='post-content']").FirstOrDefault().Descendants("p").Where(k => k.InnerText.ToLower().Contains("area") || k.InnerText.ToLower().Contains("district")).Count() != 0) ? doc.DocumentNode.SelectNodes("//div[@class='post-content']").FirstOrDefault().Descendants("p") : doc.DocumentNode.SelectNodes("//div[@class='post-content']").FirstOrDefault().Descendants("h3");
                foreach (var item in add)
                {
                    if (item.InnerText.ToLower().Contains("area") || item.InnerText.ToLower().Contains("district"))
                    {
                        var name = ParseAreaName(item.InnerText);
                        cla.Add(name);
                    }
                }
            }
            catch (Exception e)
            {

               // throw;
            }
           
            return cla;
        }

        private string ParseAreaName(string name)
        {
            try
            {
                if (name.ToLower().Contains("zip")) return name.Substring(0, name.ToLower().IndexOf("zip") - 1).Replace("Area:", "").Replace("District:", "").Trim();
                else return name.Replace("Area:", "").Replace("District:", "").Trim();
            }
            catch (Exception e)
            {

                return name;
            }
            
        }

        private string NameParser(string lgaCityName)
        {
            try
            {
                if (lgaCityName.ToLower().Contains("town"))
                {
                    return lgaCityName.Substring(0, lgaCityName.ToLower().IndexOf("area") - 1);
                }
                else
                {
                    return lgaCityName.Substring(0, lgaCityName.ToLower().IndexOf("L.G".ToLower()) - 1);
                }
            }
            catch (Exception e)
            {
                return lgaCityName;
            }
            
        }



    }




    public class CityLgaArea 
    {
        public virtual List<EntityString> Area
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Id { get; set; }

    }


    public class BaseLocation 
    {
        public virtual List<EntityString> AreaPredictions
        {
            get;
            set;
        }

        public virtual List<CityLgaArea> CityLga
        {
            get;
            set;
        }


        public int Id
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

    }




    public class EntityString
    {
        public int Id { get; set; }
        public string Value { get; set; }


        public static implicit operator EntityString(string value)
        {
            return new EntityString { Value = value };

        }

        public static bool operator ==(EntityString instance, string value)
        {
            return instance.Value == value;
        }
        public static bool operator !=(EntityString instance, string value)
        {
            return instance.Value != value;
        }

        public static string operator +(EntityString instance, string value)
        {
            return instance.Value + value;
        }

        public override string ToString()
        {
            return this.Value;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(string))
            {
                return this.Value == (string)obj;
            }
            return base.Equals(obj);
        }

        public static implicit operator string(EntityString value)
        {
            return value.Value;
        }

    }



}
